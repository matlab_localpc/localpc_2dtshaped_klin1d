% Test of KL - COV matrix


% # of samples
M = 100;
AVE_h = 10;
AVE_v = 10;

L_h = 4e-1;
L_v = 4e-1;

% kernel
k_h = @(x,y) exp(-(x-y)'*(x-y)/(L_h^2));
k_v = @(x,y) exp(-(x-y)'*(x-y)/(L_v^2));


% Quad rule
pol = OrthPolynomial.Legendre();
qr = pol.quadrule(198);
points_h = 3*[-1;qr.x';1]; % x in [-3,3], column vector
points_v = 2*[-1;qr.x';1]-1; % y in [-3,1], column vector

n_h = size(points_h,1);
n_v = size(points_v,1);

%% Horizontal part
% coveriance matrix
C_h = zeros(n_h,n_h); 
for i = 1:n_h 
    for j = 1:n_h 
        C_h(i,j) = k_h( points_h(i), points_h(j) ); 
    end
end


% only take first 40 eigenvalues to avoid negative small values
[Veig_h,Deig_h] = eig(C_h);
Ntc_h = 40;
Veig_h = Veig_h(:,end-Ntc_h+1:end);
Deig_h = Deig_h(end-Ntc_h+1:end,end-Ntc_h+1:end);

z_h = zeros(n_h,M);

for i = 1:M
    u = rand(Ntc_h,1)*2-1; 
    z_h(:,i) = AVE_h + Veig_h*sqrt(Deig_h)*u;
end

KLT2D.h.A   = z_h;
KLT2D.h.x   = points_h;
KLT2D.h.n   = n_h;
KLT2D.h.AVE = AVE_h;

%% Vertical part
% coveriance matrix
C_v = zeros(n_v,n_v); 
for i = 1:n_v 
    for j = 1:n_v 
        C_v(i,j) = k_v( points_v(i), points_v(j) ); 
    end
end


% only take first 40 eigenvalues to avoid negative small values
[Veig_v,Deig_v] = eig(C_v);
Ntc_v = 120;
Veig_v = Veig_v(:,end-Ntc_v+1:end);
Deig_v = Deig_v(end-Ntc_v+1:end,end-Ntc_v+1:end);

z_v = zeros(n_v,M);

for i = 1:M
    u = rand(Ntc_v,1)*2-1; 
    z_v(:,i) = AVE_v + Veig_v*sqrt(Deig_v)*u;
end

KLT2D.v.A   = z_v;
KLT2D.v.y   = points_v;
KLT2D.v.n   = n_v;
KLT2D.v.AVE = AVE_v;


% Show the look of sample #1
%%{
[U,V] = meshgrid(KLT2D.h.x, linspace(3,1,20));
surf_h = repmat(KLT2D.h.A(:,1)',20,1);
%surf(U,V,surf_h);
surf(U,V,surf_h,'EdgeColor', 'none');
hold on;
[U,V] = meshgrid(linspace(-1,1,40),KLT2D.v.y);
surf_v = repmat(KLT2D.v.A(:,1),1,40);
%surf(U,V,surf_v);
surf(U,V,surf_v,'EdgeColor', 'none');
zlim([0,15])
%}

%{
%plot(KLT2D.h.x,KLT2D.h.A(:,1),'-o');
%hold on;
%plot(KLT2D.v.y,KLT2D.v.A(:,1),'r-o');
%}

R(1) = max(max(KLT2D.h.A - KLT2D.h.AVE));
R(2) = min(min(KLT2D.h.A - KLT2D.h.AVE));
R(3) = max(max(KLT2D.v.A - KLT2D.v.AVE));
R(4) = min(min(KLT2D.v.A - KLT2D.v.AVE));

if max(abs(R)) >= 2.5
    disp('KLT2D problems, up and downs outside 2.5 range');
end

% save all samples
save('KLT2D_AVE10_L4e-1.mat','KLT2D');


