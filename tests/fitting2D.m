function [ norm ] = fitting2D( s )
% Test of KL with COV Expansion and fitting

% # of truncation order = s
% s = 10;

% # of samples
M = 1000;


% Quad rule
pol = OrthPolynomial.Legendre();
qr = pol.quadrule(20);
points = qr.x';


[U,V] = meshgrid(points, points);
x = [U(:) V(:)]'; 
n = size(x,2);

% load A - random field samples on all grid points
% Rows - points, Col's - samples 
load('KL2Dquad20.mat','z');
A = z;


Abar = (1/M) * sum(A,2);
Atilda = A - repmat(Abar,1,M);
C =  (1/M) * (Atilda * Atilda');


% take out first s eigenvalues and eigenvectors
[Veig,Deig] = eigs(C,s);


% Prepare least square fitting equation LSC*y = LSd
LSC = Veig * sqrt(Deig);
LSd = A(:,1);

% LS solver
y = lsqlin(LSC,LSd);

% re-construct a's approximation
Aapprox = LSC * y;

% Error
err = LSd - Aapprox;
norm = max(abs(err));


%{
figure; 
clf; 
Z1 = reshape(LSd, sqrt(n), sqrt(n)); 
surf(U,V,Z1,'faceColor','r');
hold on;
figure;
Z2 = reshape(Aapprox, sqrt(n), sqrt(n)); 
surf(U,V,Z2,'faceColor','b');
%}

end

