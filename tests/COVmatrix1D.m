% Test of KL - COV matrix -- 1D

% # of samples
M = 1000;

% kernel
k = @(x,y) exp(-10*(x-y)'*(x-y));

% Quad rule
pol = OrthPolynomial.Legendre();
qr = pol.quadrule(200);
points = qr.x';

n = size(points,1);

C = zeros(n,n); 
for i = 1:n 
    for j = 1:n 
        C(i,j) = k(points(i), points(j)); 
    end
end


[Veig,Deig] = eig(C);

z = zeros(n,M);

for i = 1:M
    u = rand(n,1)*2-1; 
    z(:,i) = Veig*sqrt(Deig)*u;
end


figure; 
plot(points,z(:,1),'-o')


save('KL1Dquad200.mat','z');