% Test of KL-COV Expansion and fitting

M = 100;

pol = OrthPolynomial.Legendre();
qr = pol.quadrule(20);

%points = (0:0.02:1)'; 
points = qr.x';

[U,V] = meshgrid(points, points);
x = [U(:) V(:)]'; 
n = size(x,2);


load('KLquad20.mat','z');
A = z;


Abar = (1/M) *sum(A,2);
Atilda = A - repmat(Abar,1,M);
C =  (1/M) * (Atilda * Atilda');

% # of truncation order
s = 10;

[Veig,Deig] = eigs(C,s);


LSC = Veig * sqrt(Deig);
LSd = A(:,1);

y = lsqlin(LSC,LSd);



Aapprox = LSC * y;

err = LSd - Aapprox;

norm = max(abs(err));

norm

figure; 
clf; 
Z1 = reshape(LSd, sqrt(n), sqrt(n)); 
surf(U,V,Z1,'faceColor','r');
hold on;
figure;
Z2 = reshape(Aapprox, sqrt(n), sqrt(n)); 
surf(U,V,Z2,'faceColor','b');
