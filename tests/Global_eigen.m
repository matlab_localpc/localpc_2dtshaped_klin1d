function [ ] = Global_eigen(  )
% Test of KL with COV Expansion and fitting

% # of truncation order = s
% s = 10;

% # of samples
M = 100;


% Quad rule

% load A - random field samples on all grid points
% Rows - points, Col's - samples 
load('KL2Dquad30all_4_L2e-1.mat','KL2D30');
A = KL2D30.A;


Abar = (1/M) * sum(A,2);
Atilda = A - repmat(Abar,1,M);
C =  (1/M) * (Atilda * Atilda');


% take out first s eigenvalues and eigenvectors
%[Veig,Deig] = eigs(C,s);
[Vall,Dall] = eig(C);


eigen = flipud(diag(abs(Dall)));

ee = eigen(1:95,1);

figure;
semilogy((1:95),ee,'k-x');

end

