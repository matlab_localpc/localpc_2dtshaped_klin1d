% Test of KL - COV matrix


% # of samples
M = 100;

L = 2e-1;

% kernel
k = @(x,y) exp(-(x-y)'*(x-y)/(L^2));
%k = @(x,y) exp(-sum(abs(x-y),1)/(L^2));

N_layer = 4;
%AVE_layer = [4,8,40,200];
AVE_layer = [4,4,40,40];

layer = linspace(-1,1,N_layer+1);

Nq_h = 30;  % horizontal
Nq_v = 10;  % vertical
pol = OrthPolynomial.Legendre();
qr_h = pol.quadrule(Nq_h-2);
qr_v = pol.quadrule(Nq_v-2);

U = cell(N_layer,1);
V = cell(N_layer,1);
z = cell(N_layer,1);
KL2Dlayer = cell(N_layer,1);

for layeridx = 1:N_layer
    l = layer(layeridx);
    r = layer(layeridx+1);
    pts_h = [-1;qr_h.x';1];
    pts_v = (l+r)/2 + ((r-l)/2)*[-1;qr_v.x';1];
    [U{layeridx},V{layeridx}] = meshgrid(pts_h, flipud(pts_v)); 
    x = [U{layeridx}(:) V{layeridx}(:)]';
    n = size(x,2);
    
    % coveriance matrix
    C = zeros(n,n); 
    for i = 1:n 
        for j = 1:n 
            C(i,j) = k(x(:,i), x(:,j)); 
        end
    end
    
    [Veig,Deig] = eig(C);
    Ntc = 150;
    Veig = Veig(:,end-Ntc+1:end);
    Deig = Deig(end-Ntc+1:end,end-Ntc+1:end);
    
    z{layeridx} = zeros(n,M);
    
    for i = 1:M
        unif = rand(Ntc,1)*2-1; 
        z{layeridx}(:,i) = AVE_layer(layeridx) + Veig*sqrt(Deig)*unif;
    end
    
    
    
    % Show the look of sample #1
    %figure; 
    %clf;
    hold on;
    surf(U{layeridx},V{layeridx},reshape(z{layeridx}(:,1), Nq_v, Nq_h));
    
    % save all samples
    KL2Dlayer{layeridx}.A = z{layeridx};
    KL2Dlayer{layeridx}.U = U{layeridx};
    KL2Dlayer{layeridx}.V = V{layeridx};
    KL2Dlayer{layeridx}.Nq_h = Nq_h;
    KL2Dlayer{layeridx}.Nq_v = Nq_v;
    KL2Dlayer{layeridx}.n = n;
    KL2Dlayer{layeridx}.AVE_layer = AVE_layer;
    
    minrange = min(min(KL2Dlayer{layeridx}.A))- KL2Dlayer{layeridx}.AVE_layer(layeridx)
    maxrange = max(max(KL2Dlayer{layeridx}.A))- KL2Dlayer{layeridx}.AVE_layer(layeridx)
    
end % layeridx



%save('KL2Dlayer_L2e-1.mat','KL2Dlayer');
save('KL2Dlayer_L2e-1_test4.mat','KL2Dlayer');