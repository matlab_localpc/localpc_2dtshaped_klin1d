function [ norm ] = fitting1D( s )
% Test of KL with COV Expansion and fitting  1D

% # of truncation order = s
% s = 10;

% # of samples
M = 1000;

% Quad rule
pol = OrthPolynomial.Legendre();
qr = pol.quadrule(200);
points = qr.x';

n = size(points,1);

% load A - random field samples on all grid points
% Rows - points, Col's - samples 
load('KL1Dquad200.mat','z');
A = z;


Abar = (1/M) * sum(A,2);
Atilda = A - repmat(Abar,1,M);
C =  (1/M) * (Atilda * Atilda');


% take out first s eigenvalues and eigenvectors
[Veig,Deig] = eigs(C,s);

% Prepare least square fitting equation LSC*y = LSd
LSC = Veig * sqrt(Deig);
LSd = A(:,1);

% LS solver
y = lsqlin(LSC,LSd);

% re-construct a's approximation
Aapprox = LSC * y;

% Error
err = LSd - Aapprox;
norm = max(abs(err));




end

