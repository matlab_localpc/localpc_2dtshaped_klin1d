function [ ] = testmesh(  )
%
%    test mash info
%
%
%
Layer.N = 4;

[ m ] = mesh_G(8, 8, 1,Layer );
m_l = mesh2d(4,4);

figure;
hold on;
for i = 1:size(m.coord,1)
    plot(m.coord(i,1),m.coord(i,2),'o');
    pname = sprintf('%d',i);
    text(m.coord(i,1),m.coord(i,2)+0.1,pname);
end %i



end

