function [ mesh ] = mesh2d( nor, noc )
%
%   mesh for [-1,1]^2
%   nor # of rows
%   noc # of cols
%
%

rowpts = linspace( 1,-1,nor+1);
colpts = linspace(-1, 1,noc+1);

[U,V] = meshgrid(colpts,rowpts);

mesh.coord = [U(:),V(:)];
mesh.ncoord = size(mesh.coord,1);

mesh.elem = zeros(nor*noc,4);
vecrow = (1:nor)';

for idxcol = 1:noc
    mesh.elem(vecrow,1) = vecrow + idxcol - 1;
    vecrow = vecrow + nor;
end %idxcol

mesh.elem(:,2) = mesh.elem(:,1) + 1;
mesh.elem(:,3) = mesh.elem(:,1) + nor + 1;
mesh.elem(:,4) = mesh.elem(:,1) + nor + 2;


mesh.bd.list{1} = (1:nor+1)';                        %Left
mesh.bd.list{2} = (nor+1:nor+1:(noc+1)*(nor+1))';    %Bottom
mesh.bd.list{3} = (1:nor+1)'+noc*(nor+1);            %Right
mesh.bd.list{4} = (1:nor+1:noc*(nor+1)+1)';          %Top

mesh.bd.all = [mesh.bd.list{1}(1:nor);...
               mesh.bd.list{2}(1:noc);...
               mesh.bd.list{3}(nor+1:-1:2);...
               mesh.bd.list{4}(noc+1:-1:2)];

mesh.bd.nbd = size(mesh.bd.all,1);

mesh.bd.allnot = (1:mesh.ncoord)';
mesh.bd.allnot(mesh.bd.all) = [];

mesh.U = U;
mesh.V = V;
mesh.nor = nor;
mesh.noc = noc;


% local boundary edges -- need this part??
% bdedge to elem
% col# 1 -- idx of elem  col # 2 -- which side 1L2B3R4T
mesh.bdetoe = zeros(2*nor+2*noc,2);
mesh.bdetoe(:,1) = [(1:nor)';(nor:nor:nor*noc)';...
                    (nor*noc:-1:nor*(noc-1)+1)';...
                    (nor*(noc-1)+1:-nor:1)'];
mesh.bdetoe(:,2) = [1*ones(nor,1);2*ones(noc,1);...
                    3*ones(nor,1);4*ones(noc,1)];   
mesh.nbde = size(mesh.bdetoe,1);

mesh.nelem = size(mesh.elem,1);

           
end

