function [ mesh ] = mesh_global_T( noe, nskl )


% big outside range [-3,3]*[-3,3]
% noe - number of elements on each sub-edge
% nskl - number of elements on each skeleton-edge 
mesh.noe = noe;
mesh.nskl = nskl;

[tmp_u,tmp_v] = meshgrid(linspace(-3,3,noe*3+1),linspace(3,1,noe+1));
mesh.coord = [tmp_u(:),tmp_v(:)];
mesh.ncoord_horizontal = size(mesh.coord,1);

[tmp_u,tmp_v] = meshgrid(linspace(-1,1,noe+1),linspace(1-2/noe,-3,2*noe));
mesh.coord(mesh.ncoord_horizontal+1:mesh.ncoord_horizontal+2*noe*(noe+1),1:2) = [tmp_u(:),tmp_v(:)];
mesh.ncoord_global = size(mesh.coord,1);
mesh.ncoord_vertical = mesh.ncoord_global - mesh.ncoord_horizontal;


% show vertices on T-shaped region
%plot(mesh.coord(:,1),mesh.coord(:,2),'o');


%% elem idx --> four vertices
mesh.elem   = zeros(noe*noe*5,4);
mesh.nelem  = noe*noe*5;
mesh.nelem_horizontal   = noe*noe*3;
mesh.nelem_vertical     = noe*noe*2;


tmp_vecrow = (1:noe)';
for idxcol = 1:noe*3
    mesh.elem(tmp_vecrow,1) = tmp_vecrow + idxcol - 1;
    tmp_vecrow = tmp_vecrow + noe;
end %idxcol

mesh.elem(1:mesh.nelem_horizontal,2) = mesh.elem(1:mesh.nelem_horizontal,1) + 1;
mesh.elem(1:mesh.nelem_horizontal,3) = mesh.elem(1:mesh.nelem_horizontal,1) + noe + 1;
mesh.elem(1:mesh.nelem_horizontal,4) = mesh.elem(1:mesh.nelem_horizontal,1) + noe + 2;

% connecting elements in vertical parts
for idx = 1:noe
    mesh.elem(mesh.nelem_horizontal+1+noe*2*(idx-1),:) = ...
        [(noe+1)^2+(idx-1)*(noe+1),...
         mesh.ncoord_horizontal+1+(idx-1)*noe*2,...
         (noe+1)^2+idx*(noe+1),...
         mesh.ncoord_horizontal+1+idx*noe*2];
end

% elements in vertival part
tmp_elemcol = (2:noe*2)';
for idxcol = 1:noe
    mesh.elem(mesh.nelem_horizontal+tmp_elemcol,1) = mesh.ncoord_horizontal + tmp_elemcol -1; 
    mesh.elem(mesh.nelem_horizontal+tmp_elemcol,2) = mesh.elem(mesh.nelem_horizontal+tmp_elemcol,1) + 1;
    mesh.elem(mesh.nelem_horizontal+tmp_elemcol,3) = mesh.elem(mesh.nelem_horizontal+tmp_elemcol,1) + noe*2;
    mesh.elem(mesh.nelem_horizontal+tmp_elemcol,4) = mesh.elem(mesh.nelem_horizontal+tmp_elemcol,1) + noe*2+1;
    tmp_elemcol = tmp_elemcol + noe*2;
end


%% Detail part

% edge -> vertex idx
mesh.edge.nh_h = (noe+1)*noe*3;
mesh.edge.nh_v = 2*noe*noe;
mesh.edge.nh   = mesh.edge.nh_h + mesh.edge.nh_v;
mesh.edge.nv_h = noe*(3*noe+1);
mesh.edge.nv_v = 2*noe*(noe+1);
mesh.edge.nv   = mesh.edge.nv_h + mesh.edge.nv_v;

mesh.edge.h = zeros(mesh.edge.nh_h + mesh.edge.nh_v ,2);
mesh.edge.v = zeros(mesh.edge.nv_h + mesh.edge.nv_v ,2);

mesh.edge.h(1:mesh.edge.nh_h,1:2) = [(1:mesh.edge.nh_h)',(1:mesh.edge.nh_h)'+noe+1];
mesh.edge.h(mesh.edge.nh_h+1:mesh.edge.nh_h+mesh.edge.nh_v,1:2) = ...
    [(mesh.edge.nh_h+noe+1+1:mesh.edge.nh_h+noe+1+mesh.edge.nh_v)',...
     (mesh.edge.nh_h+noe+1+1:mesh.edge.nh_h+noe+1+mesh.edge.nh_v)'+2*noe];
 
for i = 1:3*noe+1  %i is colidx
    mesh.edge.v((i-1)*noe+1:i*noe,1) = ((i-1)*(noe+1)+1:i*(noe+1)-1)';    
end 
mesh.edge.v(1:mesh.edge.nv_h,2) = mesh.edge.v(1:mesh.edge.nv_h,1)+1;

for i = 1:noe+1
    mesh.edge.v(mesh.edge.nv_h+(i-1)*2*noe+1,1:2) = [(noe+1)^2+(i-1)*(noe+1),mesh.ncoord_horizontal+1+(i-1)*2*noe];
end

tmp_edgerow = (2:2*noe)';
for i = 1:noe+1
    mesh.edge.v(mesh.edge.nv_h+tmp_edgerow,1:2) = ...
        [mesh.ncoord_horizontal+tmp_edgerow-1,mesh.ncoord_horizontal+tmp_edgerow];
    tmp_edgerow = tmp_edgerow + 2*noe;
end


% coord for additional skeleton points.

nver = mesh.ncoord_global;
ninside = nskl-1;
mesh.coord(nver+1:nver+ninside*(mesh.edge.nh+mesh.edge.nv),1:2) = 0;
for i = 1:mesh.edge.nh  %idx of skl points - horizontal edges
    tmpx = linspace(mesh.coord(mesh.edge.h(i,1),1),mesh.coord(mesh.edge.h(i,2),1),ninside+2)';
    mesh.coord(nver+(i-1)*ninside+1:nver+i*ninside,1) = tmpx(2:end-1);
    mesh.coord(nver+(i-1)*ninside+1:nver+i*ninside,2) = mesh.coord(mesh.edge.h(i,1),2) * ones(ninside,1);
end

for i = 1:mesh.edge.nv  %idx of skl points - vertical edges
    mesh.coord(nver+mesh.edge.nh*ninside+(i-1)*ninside+1:nver+mesh.edge.nh*ninside+i*ninside,1) = mesh.coord(mesh.edge.v(i,1),1) * ones(ninside,1);
    tmpy = linspace(mesh.coord(mesh.edge.v(i,1),2),mesh.coord(mesh.edge.v(i,2),2),ninside+2)';
    mesh.coord(nver+mesh.edge.nh*ninside+(i-1)*ninside+1:nver+mesh.edge.nh*ninside+i*ninside,2) = tmpy(2:end-1);
end

% show how I generate these coord's with path '-'
%{
plot(mesh.coord(1:mesh.ncoord_global,1),mesh.coord(1:mesh.ncoord_global,2),'-o')
plot(mesh.coord(1:mesh.ncoord_global,1),mesh.coord(1:mesh.ncoord_global,2),'o')
hold on
plot(mesh.coord(mesh.ncoord_global+1:end,1),mesh.coord(mesh.ncoord_global+1:end,2),'r-o')
%}

%% elem idx --> all skl points in this elem, counter-clockwise
mesh.elemtov = zeros(5*noe^2,nskl*4);

mesh.nelemtov = mesh.nelem;
%mesh.nelem_horizontal   = noe*noe*3;
%mesh.nelem_vertical     = noe*noe*2;

for i = 1:mesh.nelem_horizontal    
    mesh.elemtov(i,1           ) = mesh.elem(i,1);
    mesh.elemtov(i,1+(nskl)    ) = mesh.elem(i,2);
    mesh.elemtov(i,1+2*(nskl)  ) = mesh.elem(i,4);
    mesh.elemtov(i,1+3*(nskl)  ) = mesh.elem(i,3);
    mesh.elemtov(i,2:nskl               )...
        = nver + ninside*mesh.edge.nh + (i-1)*ninside + (1:ninside);        
    mesh.elemtov(i,2+1*(nskl):2*(nskl)  )...
        = nver + ( rem(i-1,noe)+(noe+1)*floor((i-1)/noe)+1 )*ninside + (1:ninside);    
    mesh.elemtov(i,2+2*(nskl):3*(nskl)  ) ...
        = nver + ninside*mesh.edge.nh + (i-1+noe)*ninside + (ninside:-1:1);    
    mesh.elemtov(i,2+3*(nskl):4*(nskl)  )...
        = nver + ( rem(i-1,noe)+(noe+1)*floor((i-1)/noe)   )*ninside + (ninside:-1:1);
end

% connecting elements in vertical part
for idx = 1:noe
    elemidx = mesh.nelem_horizontal+1+noe*2*(idx-1);
    mesh.elemtov(elemidx,:) = ...
        [mesh.elem(elemidx,1),...
         nver + ninside*mesh.edge.nh + ninside*mesh.edge.nv_h + (idx-1)*noe*2*ninside + (1:ninside),...
         mesh.elem(elemidx,2),...
         nver + ninside*mesh.edge.nh_h + (idx-1)*noe*2*ninside + (1:ninside),...
         mesh.elem(elemidx,4),...
         nver + ninside*mesh.edge.nh + ninside*mesh.edge.nv_h + (idx  )*noe*2*ninside + (ninside:-1:1),...
         mesh.elem(elemidx,3),...
         nver + ninside*((noe+1)^2-1) + ninside*(idx-1)*(noe+1) + (ninside:-1:1),...
         ];
end

% vertical part
for idxcol = 1:noe
    for elemidx = mesh.nelem_horizontal+(idxcol-1)*noe*2+2:mesh.nelem_horizontal+idxcol*noe*2
        idxrow = elemidx - (mesh.nelem_horizontal+(idxcol-1)*noe*2);
        mesh.elemtov(elemidx,:) = ...
        [mesh.elem(elemidx,1),...
         nver + ninside*mesh.edge.nh + ninside*mesh.edge.nv_h + (idxcol-1)*noe*2*ninside + (idxrow-1)*ninside + (1:ninside),...
         mesh.elem(elemidx,2),...
         nver + ninside*mesh.edge.nh_h + (idxcol-1)*noe*2*ninside + (idxrow-1)*ninside + (1:ninside),...
         mesh.elem(elemidx,4),...
         nver + ninside*mesh.edge.nh + ninside*mesh.edge.nv_h + (idxcol  )*noe*2*ninside + (idxrow-1)*ninside + (ninside:-1:1),...
         mesh.elem(elemidx,3),...
         nver + ninside*mesh.edge.nh_h + (idxcol-1)*noe*2*ninside + (idxrow-2)*ninside + (ninside:-1:1),...
         ];        
    end
end

% show all element with all skeleton points.
%{
%x = zeros(mesh.nelem_horizontal*nskl*4,1);
%y = zeros(mesh.nelem_horizontal*nskl*4,1);
x = zeros(mesh.nelem,1);
y = zeros(mesh.nelem,1);
%for i = 1:mesh.nelem_horizontal
for i = 1:mesh.nelem
    for j = 1:nskl*4        
        x((i-1)*nskl*4+j) = mesh.coord(mesh.elemtov(i,j),1);
        y((i-1)*nskl*4+j) = mesh.coord(mesh.elemtov(i,j),2);
    end
end
plot(x,y,'-o');
%}

%{
x = zeros(4*nskl*4,1);
y = zeros(4*nskl*4,1);

for j = 1:nskl*4        
    x((0)*nskl*4+j) = mesh.coord(mesh.elemtov(49,j),1);
    y((0)*nskl*4+j) = mesh.coord(mesh.elemtov(49,j),2);
end
for j = 1:nskl*4        
    x((1)*nskl*4+j) = mesh.coord(mesh.elemtov(57,j),1);
    y((1)*nskl*4+j) = mesh.coord(mesh.elemtov(57,j),2);
end
for j = 1:nskl*4        
    x((2)*nskl*4+j) = mesh.coord(mesh.elemtov(65,j),1);
    y((2)*nskl*4+j) = mesh.coord(mesh.elemtov(65,j),2);
end
for j = 1:nskl*4        
    x((3)*nskl*4+j) = mesh.coord(mesh.elemtov(73,j),1);
    y((3)*nskl*4+j) = mesh.coord(mesh.elemtov(73,j),2);
end
plot(x,y,'-o');
%}

% Global boundary idx
mesh.gbd.top    = find(mesh.coord(:,2) ==  3);
mesh.gbd.bottom = find(mesh.coord(:,2) == -3);
mesh.gbd.left   = find(mesh.coord(:,1) == -3);
mesh.gbd.right  = find(mesh.coord(:,1) ==  3);
mesh.gbd.under_roof = intersect(find(mesh.coord(:,2) == 1),find(mesh.coord(:,1) <= -1 | mesh.coord(:,1) >= 1));
mesh.gbd.col_left   = find(mesh.coord(:,1) == -1 & mesh.coord(:,2) <= 1);
mesh.gbd.col_right  = find(mesh.coord(:,1) ==  1 & mesh.coord(:,2) <= 1);

mesh.gbd.idx = unique([mesh.gbd.top;mesh.gbd.bottom;mesh.gbd.left;mesh.gbd.right;...
                     mesh.gbd.under_roof;mesh.gbd.col_left;mesh.gbd.col_right]);

mesh.gbd.idxnot = setdiff((1:size(mesh.coord,1))',mesh.gbd.idx);

mesh.gbd.nbd = size(mesh.gbd.idx,1);

% show boundary points - red.
%{
figure;
plot(mesh.coord(mesh.gbd.idx,1),mesh.coord(mesh.gbd.idx,2),'ro');
hold on;
plot(mesh.coord(mesh.gbd.idxnot,1),mesh.coord(mesh.gbd.idxnot,2),'o');
%}


% For mesh_total.
mesh.bd.top    = mesh.gbd.top(find(mesh.gbd.top <= mesh.ncoord_global));
mesh.bd.bottom = mesh.gbd.bottom(find( mesh.gbd.bottom <= mesh.ncoord_global));
mesh.bd.left   = mesh.gbd.left(find( mesh.gbd.left <= mesh.ncoord_global));
mesh.bd.right  = mesh.gbd.right(find( mesh.gbd.right <= mesh.ncoord_global));
mesh.bd.under_roof = mesh.gbd.under_roof(find( mesh.gbd.under_roof <= mesh.ncoord_global));
mesh.bd.col_left   = mesh.gbd.col_left(find( mesh.gbd.col_left <= mesh.ncoord_global));
mesh.bd.col_right  = mesh.gbd.col_right(find( mesh.gbd.col_right <= mesh.ncoord_global));

mesh.bd.all = unique([mesh.bd.top;mesh.bd.bottom;mesh.bd.left;mesh.bd.right;...
                     mesh.bd.under_roof;mesh.bd.col_left;mesh.bd.col_right]);

mesh.bd.nbd = size(mesh.bd.all,1);

mesh.bd.allnot = (1:mesh.ncoord_global)';
mesh.bd.allnot(mesh.bd.all) = [];

% show bd points
%{
figure;
plot(mesh.coord(mesh.bd.all,1),mesh.coord(mesh.bd.all,2),'o');
hold on;
plot(mesh.coord(mesh.bd.allnot,1),mesh.coord(mesh.bd.allnot,2),'ro');
%}


end