% Show mesh


% number of subdomains (on both dim) in one square region out of five in T-shaped region
noe  = 4;

% number of skeleton elements (on both dim) in each subdomain
nskl = 4;


[ mesh ] = mesh_global_T( noe, nskl );


% show all element with all skeleton points.
%{
%x = zeros(mesh.nelem_horizontal*nskl*4,1);
%y = zeros(mesh.nelem_horizontal*nskl*4,1);
x = zeros(mesh.nelem,1);
y = zeros(mesh.nelem,1);
%for i = 1:mesh.nelem_horizontal
for i = 1:mesh.nelem
    for j = 1:nskl*4        
        x((i-1)*nskl*4+j) = mesh.coord(mesh.elemtov(i,j),1);
        y((i-1)*nskl*4+j) = mesh.coord(mesh.elemtov(i,j),2);
    end
end
plot(x,y,'.');
%}


% show boundary points - red.
%%{
figure;
plot(mesh.coord(mesh.gbd.idx,1),mesh.coord(mesh.gbd.idx,2),'r.');
hold on;
plot(mesh.coord(mesh.gbd.idxnot,1),mesh.coord(mesh.gbd.idxnot,2),'kx');
xlim([-4,4])
ylim([-4,4])
%}