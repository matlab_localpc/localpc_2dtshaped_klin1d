function [Aloc, b] = STIMA_inter_FE( Vertices, mesh_global, mesh_local, elemidx, solPCbd, idxsamp, YList, ainfo, ginfo, FEinfo )
%
%
%
%
%
%
%
%

PM.vidx = mesh_global.elem(elemidx,:);
PM.vcoord = mesh_global.coord(PM.vidx,:);
PM.const = (1/4) * (PM.vcoord(1,2) - PM.vcoord(2,2)) * (PM.vcoord(4,1) - PM.vcoord(1,1));

%% TO BE EDIT
YListone  = YList{elemidx} ;  % row vector's    

%% interface Claude
%%{

M = assem_Mat( mesh_local, @STIMA_FE, YListone, elemidx, ainfo, ginfo.int, FEinfo, PM, idxsamp);
%ainfo, ginfo.int, FEinfo, PM ;

%Mbd = assem_Mat_bd(mesh_local, @STIMA_bd, YListone, elemidx, ainfo, ginfo.lint, FEinfo, PM);

Aloc = solPCbd{elemidx}(:,1:mesh_local.bd.nbd, idxsamp)' * M * solPCbd{elemidx}(:,1:mesh_local.bd.nbd, idxsamp);
%Aloc = solPCbd{elemidx}(:,1:mesh_local.bd.nbd, idxsamp)' * (M-Mbd) * solPCbd{elemidx}(:,1:mesh_local.bd.nbd, idxsamp);

F  = assem_F( mesh_local, @STIMA_F, ginfo.int, FEinfo, PM );

b = solPCbd{elemidx}(:,1:mesh_local.bd.nbd, idxsamp)' * (F - M * solPCbd{elemidx}(:,mesh_local.bd.nbd+1,idxsamp));
%b =  solPCbd{elemidx}(:,1:mesh_local.bd.nbd, idxsamp)' * ( F - (M-Mbd) * solPCbd{elemidx}(:,mesh_local.bd.nbd+1,idxsamp) );


%}



%%
%{
M = assem_Mat( mesh_local, @STIMA, YListone, elemidx, ainfo, ginfo.int, FEinfo, PM);
%ainfo, ginfo.int, FEinfo, PM ;

Mbd = assem_Mat_bd(mesh_local, @STIMA_bd, YListone, elemidx, ainfo, ginfo.lint, FEinfo, PM);


Aloc = (solPCbd{elemidx}(:,1:mesh_local.bd.nbd, idxsamp))' * (M-Mbd) * solPCbd{elemidx}(:,1:mesh_local.bd.nbd, idxsamp);

F  = assem_F( mesh_local, @STIMA_F, ginfo.int, FEinfo, PM );

b = (solPCbd{elemidx}(:,1:mesh_local.bd.nbd, idxsamp))' * (F - (M-Mbd) * solPCbd{elemidx}(:,mesh_local.bd.nbd+1,idxsamp));
%}

%%
%{

M = assem_Mat( mesh_local, @STIMA, YListone, elemidx, ainfo, ginfo.int, FEinfo, PM);
%ainfo, ginfo.int, FEinfo, PM ;

Mbd = assem_Mat_bd(mesh_local, @STIMA_bd, YListone, elemidx, ainfo, ginfo.lint, FEinfo, PM);



Aloc = (M(mesh_local.bd.all,:)-Mbd(mesh_local.bd.all,:)) * solPCbd{elemidx}(:,1:mesh_local.bd.nbd, idxsamp);
%Aloc = solPCbd{elemidx}(:,1:mesh_local.bd.nbd, idxsamp)' * (M-Mbd) * solPCbd{elemidx}(:,1:mesh_local.bd.nbd, idxsamp);

%F  = assem_F( mesh_local, @STIMA_F, ginfo.int, FEinfo, PM );

b =  F(mesh_local.bd.all,1) - (M(mesh_local.bd.all,:)-Mbd(mesh_local.bd.all,:)) * solPCbd{elemidx}(:,mesh_local.bd.nbd+1,idxsamp);
%b =  0 - (M(mesh_local.bd.all,:)-Mbd(mesh_local.bd.all,:)) * solPCbd{elemidx}(:,mesh_local.bd.nbd+1,idxsamp);


%b =  solPCbd{elemidx}(:,1:mesh_local.bd.nbd, idxsamp)' * ( F - (M-Mbd) * solPCbd{elemidx}(:,mesh_local.bd.nbd+1,idxsamp) );
%}


%% interface 0
%%{

%M = assem_Mat( mesh_local, @STIMA, YListone, elemidx, ainfo, ginfo.int, FEinfo, PM);
%ainfo, ginfo.int, FEinfo, PM ;

%Mbd = assem_Mat_bd(mesh_local, @STIMA_bd_FE, YListone, elemidx, ainfo, ginfo.lint, FEinfo, PM);

%Aloc = Mbd(mesh_local.bd.all,:) * solPCbd{elemidx}(:,1:mesh_local.bd.nbd, idxsamp);
%Aloc = solPCbd{elemidx}(:,1:mesh_local.bd.nbd, idxsamp)' * (M-Mbd) * solPCbd{elemidx}(:,1:mesh_local.bd.nbd, idxsamp);

%F  = assem_F( mesh_local, @STIMA_F, ginfo.int, FEinfo, PM );

%b =  0 - Mbd(mesh_local.bd.all,:) * solPCbd{elemidx}(:,mesh_local.bd.nbd+1,idxsamp);
%b =  solPCbd{elemidx}(:,1:mesh_local.bd.nbd, idxsamp)' * ( F - (M-Mbd) * solPCbd{elemidx}(:,mesh_local.bd.nbd+1,idxsamp) );


%}


end


