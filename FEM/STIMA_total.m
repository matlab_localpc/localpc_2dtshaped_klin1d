function Aloc = STIMA_total( Vertices, elemidx, ainfo, ginfo, FEinfo, mapsub, idxsamp )
%
%
%   ginfo = ginfo.int from input
%
%
%



V1 = Vertices(1,:);
V2 = Vertices(2,:);
V3 = Vertices(3,:);
%V4 = Vertices(4,:);

acoord = zeros(ginfo.N, 2);
acoord(:,1) = (V1(1)+V3(1))/2 + ((V3(1)-V1(1))/2) * ginfo.grid(:,1);
acoord(:,2) = (V1(2)+V2(2))/2 + ((V1(2)-V2(2))/2) * ginfo.grid(:,2);


% Vq = interp2(X,Y,V,Xq,Yq) 2D interpolation
%{
SV = ainfo.Abar{mapsub(elemidx)} + ainfo.LSmat{mapsub(elemidx)} * ainfo.Range * Y{mapsub(elemidx)}';  % sample value in one col vector
if max(SV<=0)==1
    find(SV<=0)
    disp('end*****');
end
SVM = reshape(SV,ainfo.Nq,ainfo.Nq);
%SVM = 1*ones(ainfo.Nq,ainfo.Nq);
aeval = interp2(ainfo.U{mapsub(elemidx)}, ainfo.V{mapsub(elemidx)}, SVM, acoord(:,1), acoord(:,2));
%}

%{
SV = ainfo.A{mapsub(elemidx)}(:,idxsamp);
SVM = reshape(SV,ainfo.Nq,ainfo.Nq);
aeval = interp2(ainfo.U{mapsub(elemidx)}, ainfo.V{mapsub(elemidx)}, SVM, acoord(:,1), acoord(:,2));
%}

SV = ainfo.A{mapsub(elemidx)}(:,idxsamp);
if mapsub(elemidx) <= ainfo.nelem_horizontal
    aeval = interp1(ainfo.U{mapsub(elemidx)}(1,:), SV, acoord(:,1));
else
    aeval = interp1(ainfo.V{mapsub(elemidx)}(:,1), SV, acoord(:,2));
end




Aloc = zeros(4,4);
Aloc(1,1) = ginfo.w' * ( aeval .* sum( FEinfo.FEd{1}(ginfo.grid) .* FEinfo.FEd{1}(ginfo.grid), 2 ) );
Aloc(1,2) = ginfo.w' * ( aeval .* sum( FEinfo.FEd{1}(ginfo.grid) .* FEinfo.FEd{2}(ginfo.grid), 2 ) );
Aloc(1,3) = ginfo.w' * ( aeval .* sum( FEinfo.FEd{1}(ginfo.grid) .* FEinfo.FEd{3}(ginfo.grid), 2 ) );
Aloc(1,4) = ginfo.w' * ( aeval .* sum( FEinfo.FEd{1}(ginfo.grid) .* FEinfo.FEd{4}(ginfo.grid), 2 ) );

Aloc(2,2) = ginfo.w' * ( aeval .* sum( FEinfo.FEd{2}(ginfo.grid) .* FEinfo.FEd{2}(ginfo.grid), 2 ) );
Aloc(2,3) = ginfo.w' * ( aeval .* sum( FEinfo.FEd{2}(ginfo.grid) .* FEinfo.FEd{3}(ginfo.grid), 2 ) );
Aloc(2,4) = ginfo.w' * ( aeval .* sum( FEinfo.FEd{2}(ginfo.grid) .* FEinfo.FEd{4}(ginfo.grid), 2 ) );

Aloc(3,3) = ginfo.w' * ( aeval .* sum( FEinfo.FEd{3}(ginfo.grid) .* FEinfo.FEd{3}(ginfo.grid), 2 ) );
Aloc(3,4) = ginfo.w' * ( aeval .* sum( FEinfo.FEd{3}(ginfo.grid) .* FEinfo.FEd{4}(ginfo.grid), 2 ) );

Aloc(4,4) = ginfo.w' * ( aeval .* sum( FEinfo.FEd{4}(ginfo.grid) .* FEinfo.FEd{4}(ginfo.grid), 2 ) );

Aloc(2,1) = Aloc(1,2);
Aloc(3,1) = Aloc(1,3);
Aloc(3,2) = Aloc(2,3);
Aloc(4,1) = Aloc(1,4);
Aloc(4,2) = Aloc(2,4);
Aloc(4,3) = Aloc(3,4);


%Aloc = Aloc / ((V1(2)-V2(2))*(V3(1)-V1(1))*(1/4) * PM.const);


end

