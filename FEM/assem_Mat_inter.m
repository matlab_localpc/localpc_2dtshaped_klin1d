function [MATinter, bb ] = assem_Mat_inter( mesh_global, mesh_local, Ehandle, solPCbd, idxsamp, YList, ainfo, ginfo, FEinfo )
%
%
%
%
%


% Initialize constants
nelem  = size(mesh_global.elem, 1);
ncoord = size(mesh_global.coord,1);
nlistsize = ((mesh_global.nskl)*4)^2;
  
% Preallocate memory

I = zeros(nlistsize*nelem,1);
J = zeros(nlistsize*nelem,1);
A = zeros(nlistsize*nelem,1);
    
bb = zeros(ncoord,1);
%FF = zeros(ncoord,1);

% Assemble element contributions

loc = 1:nlistsize;
for i = 1:nelem

% Extract vertices of current element

idx = mesh_global.elemtov(i,:);
Vertices = mesh_global.coord(idx,:);

% Compute element contributions

[Aloc,b] = Ehandle(Vertices,mesh_global,mesh_local,i,solPCbd,idxsamp,YList,ainfo, ginfo, FEinfo);

% Add contributions to stiffness matrix

I(loc) = set_Rows(idx,(mesh_global.nskl)*4);
J(loc) = set_Cols(idx,(mesh_global.nskl)*4);
A(loc) = Aloc(:);
loc = loc+nlistsize;


bb(idx,1) = bb(idx,1) + b;
%FF(idx,1) = FF(idx,1) + F;

end % for

MATinter = sparse(I,J,A,ncoord,ncoord);

% Assign output arguments
%if(nargout == 2)
%    varargout{1} = sparse(I,J,A,ncoord,ncoord);
%    varargout{2} = bb;
%else





end % function
