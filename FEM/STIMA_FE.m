function Aloc = STIMA_FE( Vertices, elemidx, Y, ainfo, ginfo, FEinfo, PM, idxsamp )
%
%
%   ginfo = ginfo.int from input
%
%
%

%%%%%%%%%%%%%%%%%%%%%%%%%%%
%idxsamp = 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%

PV1 = PM.vcoord(1,:);
PV2 = PM.vcoord(2,:);
%PV3 = PM.vcoord(3,:);
PV4 = PM.vcoord(4,:);


V1 = Vertices(1,:);
V2 = Vertices(2,:);
V3 = Vertices(3,:);
%V4 = Vertices(4,:);

acoord = zeros(ginfo.N, 2);
acoord(:,1) = (PV1(1)+PV4(1))/2 + ((PV4(1)-PV1(1))/2) *... 
              (  (V1(1)+V3(1))/2 + ((V3(1)-V1(1))/2) * ginfo.grid(:,1)  );
acoord(:,2) = (PV1(2)+PV2(2))/2 + ((PV1(2)-PV2(2))/2) *...
              (  (V1(2)+V2(2))/2 + ((V1(2)-V2(2))/2) * ginfo.grid(:,2)  );


% Vq = interp2(X,Y,V,Xq,Yq) 2D interpolation
%SV = ainfo.Abar + ainfo.LSmat * Y';  % sample value in one col vector
%SVM = reshape(SV,ainfo.Nq,ainfo.Nq);
%aeval = interp2(ainfo.grid.U, ainfo.grid.V, SVM, acoord(:,1), acoord(:,2));
SV = ainfo.A{elemidx}(:,idxsamp);
SVM = reshape(SV,ainfo.Nq,ainfo.Nq);
aeval = interp2(ainfo.U{elemidx}, ainfo.V{elemidx}, SVM, acoord(:,1), acoord(:,2));


Aloc = zeros(4,4);
Aloc(1,1) = ginfo.w' * ( aeval .* sum( FEinfo.FEd{1}(ginfo.grid) .* FEinfo.FEd{1}(ginfo.grid), 2 ) );
Aloc(1,2) = ginfo.w' * ( aeval .* sum( FEinfo.FEd{1}(ginfo.grid) .* FEinfo.FEd{2}(ginfo.grid), 2 ) );
Aloc(1,3) = ginfo.w' * ( aeval .* sum( FEinfo.FEd{1}(ginfo.grid) .* FEinfo.FEd{3}(ginfo.grid), 2 ) );
Aloc(1,4) = ginfo.w' * ( aeval .* sum( FEinfo.FEd{1}(ginfo.grid) .* FEinfo.FEd{4}(ginfo.grid), 2 ) );

Aloc(2,2) = ginfo.w' * ( aeval .* sum( FEinfo.FEd{2}(ginfo.grid) .* FEinfo.FEd{2}(ginfo.grid), 2 ) );
Aloc(2,3) = ginfo.w' * ( aeval .* sum( FEinfo.FEd{2}(ginfo.grid) .* FEinfo.FEd{3}(ginfo.grid), 2 ) );
Aloc(2,4) = ginfo.w' * ( aeval .* sum( FEinfo.FEd{2}(ginfo.grid) .* FEinfo.FEd{4}(ginfo.grid), 2 ) );

Aloc(3,3) = ginfo.w' * ( aeval .* sum( FEinfo.FEd{3}(ginfo.grid) .* FEinfo.FEd{3}(ginfo.grid), 2 ) );
Aloc(3,4) = ginfo.w' * ( aeval .* sum( FEinfo.FEd{3}(ginfo.grid) .* FEinfo.FEd{4}(ginfo.grid), 2 ) );

Aloc(4,4) = ginfo.w' * ( aeval .* sum( FEinfo.FEd{4}(ginfo.grid) .* FEinfo.FEd{4}(ginfo.grid), 2 ) );

Aloc(2,1) = Aloc(1,2);
Aloc(3,1) = Aloc(1,3);
Aloc(3,2) = Aloc(2,3);
Aloc(4,1) = Aloc(1,4);
Aloc(4,2) = Aloc(2,4);
Aloc(4,3) = Aloc(3,4);


%Aloc = Aloc / ((V1(2)-V2(2))*(V3(1)-V1(1))*(1/4) * PM.const);


end

