function varargout = assem_Mat_total( mesh, Ehandle, varargin )
%
%
%
%
%


% Initialize constants
nelem  = size(mesh.elem, 1);
ncoord = mesh.ncoord_global;
  
% Preallocate memory

I = zeros(16*nelem,1);
J = zeros(16*nelem,1);
A = zeros(16*nelem,1);
    
% Assemble element contributions

loc = 1:16;
for i = 1:nelem

% Extract vertices of current element

idx = mesh.elem(i,:);
Vertices = mesh.coord(idx,:);

% Compute element contributions

Aloc = Ehandle(Vertices, i, varargin{:});

% Add contributions to stiffness matrix

I(loc) = set_Rows(idx,4);
J(loc) = set_Cols(idx,4);
A(loc) = Aloc(:);
loc = loc+16;

end % for

% Assign output arguments

if(nargout > 1)
    varargout{1} = I;
    varargout{2} = J;
    varargout{3} = A;
else
    varargout{1} = sparse(I,J,A,ncoord,ncoord);      % sparse will add up all values in the same entry.     
end % if
  
  

end % function

