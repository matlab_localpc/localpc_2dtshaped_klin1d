function Aloc = STIMA_bd( Vertices, elemidx,  Y, direc, idxvert, ainfo, ginfo, FEinfo, PM )
%
%
%   ginfo = ginfo.int from input
%
%
%


PV1 = PM.vcoord(1,:);
PV2 = PM.vcoord(2,:);
PV3 = PM.vcoord(3,:);
PV4 = PM.vcoord(4,:);


V1 = Vertices(1,:);
V2 = Vertices(2,:);
V3 = Vertices(3,:);
V4 = Vertices(4,:);

acoord = zeros(ginfo.N, 2);

SV = ainfo.Abar{elemidx} + ainfo.LSmat{elemidx} * ainfo.Range * Y';  % sample value in one col vector
if max(SV<=0)==1
    find(SV<=0)
    disp('end*****');
end
SVM = reshape(SV,ainfo.Nq,ainfo.Nq);
%SVM = 1*ones(ainfo.Nq,ainfo.Nq);



switch direc
    case 1 % left
        acoord(:,1) = PV1(1);
        acoord(:,2) = (PV1(2)+PV2(2))/2 + ((PV1(2)-PV2(2))/2) *... 
                      (  (V1(2)+V2(2))/2 + ((V1(2)-V2(2))/2) * ginfo.grid  );
        aeval = interp2(ainfo.U{elemidx}, ainfo.V{elemidx}, SVM, acoord(:,1), acoord(:,2));
        nvec = [-1,0];
        coordlocal = [-1*ones(size(ginfo.grid,1),1), (ginfo.grid)];
        idxrow = [1,2];
        %direction indicator for line integral
        %dirconst = -1;
        
    case 2 % Bottom
        acoord(:,1) = (PV2(1)+PV4(1))/2 + ((PV4(1)-PV2(1))/2) *... 
                      (  (V2(1)+V4(1))/2 + ((V4(1)-V2(1))/2) * ginfo.grid  );
        acoord(:,2) = PV2(2);
        aeval = interp2(ainfo.U{elemidx}, ainfo.V{elemidx}, SVM, acoord(:,1), acoord(:,2));
        nvec = [0,-1];
        coordlocal = [ginfo.grid,-1*ones(size(ginfo.grid,1),1)];       
        idxrow = [2,4];
        %dirconst =  1;

    case 3 % Right
        acoord(:,1) = PV3(1);
        acoord(:,2) = (PV3(2)+PV4(2))/2 + ((PV4(2)-PV3(2))/2) *... 
                      (  (V3(2)+V4(2))/2 + ((V4(2)-V3(2))/2) * ginfo.grid  );
        aeval = interp2(ainfo.U{elemidx}, ainfo.V{elemidx}, SVM, acoord(:,1), acoord(:,2));
        nvec = [ 1,0];
        coordlocal = [ 1*ones(size(ginfo.grid,1),1), ginfo.grid];
        idxrow = [3,4];
        %dirconst =  1;

        
    case 4 % Top
        acoord(:,1) = (PV1(1)+PV3(1))/2 + ((PV3(1)-PV1(1))/2) *... 
                      (  (V1(1)+V3(1))/2 + ((V3(1)-V1(1))/2) * ginfo.grid  );
        acoord(:,2) = PV1(2);
        aeval = interp2(ainfo.U{elemidx}, ainfo.V{elemidx}, SVM, acoord(:,1), acoord(:,2));
        nvec = [0, 1];
        coordlocal = [(ginfo.grid), 1*ones(size(ginfo.grid,1),1)];  
        idxrow = [1,3];
        %dirconst = -1;

end

%aeval(:) = 1;

%{
a = 0.01;
dd = @(v) (1/(a*(pi^1/2))) * exp((-v(:,1).^2-v(:,2).^2)/(a^2));

FEinfo.FEdelta{1} = @(v) dd([v(:,1)+1,v(:,1)-1]);
FEinfo.FEdelta{2} = @(v) dd([v(:,1)+1,v(:,1)+1]);
FEinfo.FEdelta{3} = @(v) dd([v(:,1)-1,v(:,1)-1]);
FEinfo.FEdelta{4} = @(v) dd([v(:,1)-1,v(:,1)+1]);
%}

Aloc = zeros(2,4);
for i = 1:2
    for j = 1:4
        Aloc(i,j) =  ginfo.w' * (aeval .* (FEinfo.FEd{j}(coordlocal) * nvec') ...
                                 .* FEinfo.FE{idxrow(i)}(coordlocal) );                
    end %j
end%i


     




%Aloc = Aloc / ((V1(2)-V2(2))*(V3(1)-V1(1))*(1/4) * PM.const);


end

