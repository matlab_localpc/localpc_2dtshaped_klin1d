function varargout = assem_Mat_bd( mesh, Ehandle, Y, elemidx, varargin )
%
%  mesh is local mesh
%
%
%


% Initialize constants
nbde  = mesh.nbde;
ncoord = size(mesh.coord,1);
  
% Preallocate memory

I = zeros(8*nbde,1);
J = zeros(8*nbde,1);
A = zeros(8*nbde,1);
    
% Assemble element contributions

loc = 1:8;
for i = 1:nbde
% Extract vertices of current element

idxelemlocal = mesh.bdetoe(i,1);
direc        = mesh.bdetoe(i,2);
idxvert   = mesh.elem(idxelemlocal,:);
Vertices     = mesh.coord(idxvert,:);

% Compute element contributions

Aloc = Ehandle(Vertices,elemidx,Y,direc,idxvert, varargin{:});

% Add contributions to stiffness matrix

% 2(v test function) * 4 (u solution function)
switch direc
    case 1 % left
        idxrow = idxvert([1,2]);
    case 2 % Bottom
        idxrow = idxvert([2,4]);
    case 3 % Right
        idxrow = idxvert([3,4]);
    case 4
        idxrow = idxvert([1,3]);
end
 
I(loc) = set_Rows(idxrow,4);
idxcol = idxvert;
J(loc) = set_Cols(idxcol,2);

A(loc) = Aloc(:);
loc = loc+8;

end % for

% Assign output arguments

if(nargout > 1)
    varargout{1} = I;
    varargout{2} = J;
    varargout{3} = A;
else
    varargout{1} = sparse(I,J,A,ncoord,ncoord);      % sparse will add up all values in the same entry.     
end % if
  
  

end % function

