function [F] = assem_F( mesh, Ehandle, varargin )
%
%
%
%
%


% Initialize constants
nelem  = size(mesh.elem, 1);
ncoord = size(mesh.coord,1);
  
% Preallocate memory
F = zeros(ncoord,1);
    
% Assemble element contributions
%loc = 1:16;
for i = 1:nelem

% Extract vertices of current element

idx = mesh.elem(i,:);
Vertices = mesh.coord(idx,:);

% Compute element contributions

Floc = Ehandle(Vertices,varargin{:});

% Add contributions to stiffness matrix

F(idx) = F(idx) + Floc;

end % for

% Assign output arguments
  
  

end % function

