function Floc = STIMA_F_total( Vertices, ginfo, FEinfo )
%
%
%
%
%
%


V1 = Vertices(1,:);
V2 = Vertices(2,:);
%V3 = Vertices(3,:);
V4 = Vertices(4,:);

acoord = zeros(ginfo.N, 2);

acoord(:,1) = (V1(1)+V4(1))/2 + ((V4(1)-V1(1))/2) * ginfo.grid(:,1);
acoord(:,2) = (V1(2)+V2(2))/2 + ((V1(2)-V2(2))/2) * ginfo.grid(:,2);

% Vq = interp2(X,Y,V,Xq,Yq) 2D interpolation

Floc = zeros(4,1);
for i = 1:4
    Floc(i,1) = ginfo.w' * ( FEinfo.f(acoord) .* FEinfo.FE{i}(ginfo.grid) );
end

Floc = Floc * (V1(2)-V2(2))*(V4(1)-V1(1))*(1/4); 




end


