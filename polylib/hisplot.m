function hisplot(y, Nbin, s, width)
%
% hisplot   -  Plot the histogram plot of vector y in line
%              Nbin = max(Nbin,20) is number of bins used.
% hisplot(y, Nbin, s, width)
     %  s  -- line spec;  width -- line width;
%
Nbin = max(Nbin,20);
width=max(width,1);

[num,x]=hist(y, Nbin);

L = length(y)*(x(2)-x(1));

plot(x, num/L, s,'linewidth',width);
