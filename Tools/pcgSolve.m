function [u,err,i] = pcgSolve(u,f,tol,apply,prec,dupair,plusp,plusd)
%PCGSOLVE preconditioned conjugate gradient iteration
%   Detailed explanation goes here

% C.J. Gittelson / SAM ETHZ / 2010

  % extract tolerance squared
  tol2 = tol^2;

  % initialize
  Av = apply(u);
  r = plusd(f,-1,Av);        
  s = prec(r);               
  v = s;
  e0 = dupair(r,s);
  i = 0;
  
  % iterate
  while(e0>tol2 && i<10000)
    i = i+1;
    
    %%%
    %i
    %%%
    
    Av = apply(v);
    va = dupair(Av,v);
    
    w = e0/va;
    
    u = plusp(u,w,v);        
    r = plusd(r,-w,Av);
    s = prec(r);
    
    e1 = dupair(r,s);
    v = plusp(s,e1/e0,v);
    e0 = e1;
    
  end
  
  % compute iteration error
  err = sqrt(e0);

end

