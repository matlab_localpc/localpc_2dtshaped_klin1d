classdef ivec < handle
%IVEC infinite vector (handle class)
%   Detailed explanation goes here
  
% C.J. Gittelson / SAM ETHZ / 2010

  properties (SetAccess=protected)
    generator % function handle for generating new values
    scalar = true; % flag for vector or cell array structure
  end

  properties (GetAccess=protected, SetAccess=protected)
    vec % store values
    len % size of vec
    comp % logical index for computed values of vec
  end
  
  methods
    function v = ivec(generator,varargin)
    % class constructor
      v = v@handle();
      v.generator = generator;
      for i=1:floor(length(varargin)/2)
        v.(varargin{2*i-1}) = varargin{2*i};
      end
      if(v.scalar)
        v.vec = nan(1);
      else
        v.vec = cell(1);
      end
      v.len = 1;
      v.comp = false;
    end
    
    function x = get(v,n)
    % get n-th element of ivec v

      % check positivity of n
      if(n<=0)
        x = 0;
        return
      end
    
      % enlarge vector to next higher power of two
      if(n>v.len)
        len1 = 2^(ceil(log2(double(n))));
        v.comp(len1) = false;
        if(v.scalar)
          v.vec(len1) = nan;
        else
          v.vec{len1} = [];
        end
        v.len = len1;
      end
      
      % construct n-th element if needed
      if(~v.comp(n))
        if(v.scalar)
          v.vec(n) = v.generator(n);
        else
          v.vec{n} = v.generator(n);
        end
        v.comp(n) = true;
      end
      
      % return desired value
      if(v.scalar)
        x = v.vec(n);
      else
        x = v.vec{n};
      end      
      
    end
    
  end
  
end

