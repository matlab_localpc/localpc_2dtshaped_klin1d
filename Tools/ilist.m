classdef ilist < handle
%ILIST dynamic list
%   Detailed explanation goes here
  
% C.J. Gittelson / SAM ETHZ / 2010
  
  properties (SetAccess=protected)
    elements = cell(1);
    N = 0;
    L = 1;
  end
  
  methods
    
    function list = ilist(varargin)
    % class constructor
      list = list@handle();    
    end
    
    function add(list,obj)
    % append element to list
      
      % enlarge vector of elements if necessary
      if(list.N==list.L)
        list.L = 2*list.L;
        list.elements{list.L} = [];
      end
      
      % append object to list
      list.N = list.N+1;
      list.elements{list.N} = obj;
    
    end
    
    
    function elem = get(list,ind)
    % get a certain element from the list
    
      elem = list.elements{ind};
      
    end
    
    function elem = getlast(list)
    % get last element added to the list
    
      elem = list.elements{list.N};
    
    end
    
    
    function elems = read(list)
    % read all elements of the list
    
      elems = list.elements(1:list.N);
    
    end
    
    
    function reset(list)
    % clear the list
    
      list.elements = cell(1);
      list.N = 0;
      list.L = 1;
    
    end
      
    
  end
  
end

