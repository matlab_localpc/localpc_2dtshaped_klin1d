classdef dvec < handle
  %DVEC dynamic vector
  %   Detailed explanation goes here
  
% C.J. Gittelson / SAM ETHZ / 2010
  
  properties (GetAccess=protected, SetAccess=protected)
    vec = zeros(1); % store values
    len = 1; % size of vec
    comp = false; % logical index for computed values of vec
  end
  
  methods
    
    function [x,flag] = get(v,n)
    % get n-th element of dvec v, return 0 if not set
    
      % check if element is set
      if(n>v.len || ~v.comp(n)) % element not in vector
        flag = false;
        x = 0;
      else % element in vector
        flag = true;
        x = v.vec(n);
      end
    end
    
    
    function set(v,n,x)
    % set n-th element to x
    
       % enlarge vector to next higher power of two
      if(n>v.len)
        len1 = 2^(ceil(log2(n)));
        v.comp(len1) = false;
        v.vec(len1) = 0;
        v.len = len1;
      end
      
      % set value
      v.vec(n) = x;
      v.comp(n) = true;
    end
      
    function x = augment(v,n,dx)
    % add x to n-th element
      x0 = v.get(n);
      x = x0 + dx;
      v.set(n,x);
    end
    
    function N = active(v)
    % get active indices
      N = find(v.comp);
    end
    
    function disp(v)
    % display vector
      disp(v.vec);    
    end
    
    function clear(v)
    % empty vector
      v.vec = zeros(1); % store values
      v.len = 1; % size of vec
      v.comp = false; % logical index for computed values of vec
    end
    
    function vec = read(v,n)
    % returns vector
      if(nargin<2 || isempty(n))
        n = find(v.comp, true, 'last');
      end
      vec = v.vec(1:n);
    end
    
  end
  
end

