function [ PCS_re_t ] = FEreconstruct( solinter, mesh_global, mesh_local, YList, ainfo, ginfo, FEinfo, idxsamp )
%
%     
%     
%   PC solution reconstruction by local FE with approximated *a*
%
%
%
%

Nsample = 100;



%% localsolver FE
solloc = cell(mesh_global.nelem,1);

for elemidx = 1:mesh_global.nelem
    PM.vidx = mesh_global.elem(elemidx,:);
    PM.vcoord = mesh_global.coord(PM.vidx,:);
    PM.const = (1/4) * (PM.vcoord(1,2) - PM.vcoord(2,2)) * (PM.vcoord(4,1) - PM.vcoord(1,1));

    solloc{elemidx}  = zeros(mesh_local.ncoord, mesh_local.bd.nbd + 1, Nsample);
    % mesh.bf.nbd + 1 : f =0 + bdprob and f + 0 bdprob

    % define local profile function F no dependence on RF
    F  = assem_F( mesh_local, @STIMA_F, ginfo.int, FEinfo, PM );
    F0 = zeros(mesh_local.ncoord,1); 
    
    MAT = assem_Mat( mesh_local, @STIMA, YList{elemidx}(idxsamp,:), elemidx, ainfo, ginfo.int, FEinfo, PM );

    for idxbdprob = 1:mesh_local.bd.nbd
        idx1 = mesh_local.bd.all(idxbdprob);
        bdv = zeros(mesh_local.ncoord,1);
        bdv(idx1) = 1;
        % F - MAT * bdcond vector
        FF = F0 - MAT * bdv; 
        % Solve non-bd solutions
        solloc{elemidx}(mesh_local.bd.allnot,idxbdprob,idxsamp) = MAT(mesh_local.bd.allnot,mesh_local.bd.allnot)\FF(mesh_local.bd.allnot,1);
        solloc{elemidx}(idx1                ,idxbdprob,idxsamp) = 1;     
    end %idxbdprob
    solloc{elemidx}(mesh_local.bd.allnot,mesh_local.bd.nbd+1,idxsamp) = MAT(mesh_local.bd.allnot,mesh_local.bd.allnot)\F(mesh_local.bd.allnot,1);
    solloc{elemidx}(mesh_local.bd.all   ,mesh_local.bd.nbd+1,idxsamp) = 0;

end % elemidx


%% build local solution from interface solution
PCSall = cell(mesh_global.nelem,1);
for elemidx = 1:mesh_global.nelem
    soltmp = solloc{elemidx}(:,1:mesh_local.bd.nbd,idxsamp) * solinter(mesh_global.elemtov(elemidx,:))...
                       + solloc{elemidx}(:,mesh_local.bd.nbd+1,idxsamp); 
    PCSall{elemidx} = reshape(soltmp,mesh_local.nor+1,mesh_local.noc+1);
end % elemidx

[ PCS_re_t ] = switch_size( PCSall, mesh_global );

%vec_x = linspace(-1,1,mesh_global.nor*mesh_local.nor+1);
%vec_y = linspace(1,-1,mesh_global.noc*mesh_local.noc+1);
%[U_total,V_total] = meshgrid(vec_x,vec_y);

%hold on;
%figure;

%scatter3(U_total(:),V_total(:),PCS_re_t(:),'g');
%title('PC solution reconstructed - approximated a');


end % function

