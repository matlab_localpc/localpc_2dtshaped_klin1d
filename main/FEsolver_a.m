function [ FES, FESall_t ] = FEsolver_a( mesh_global, mesh_local, ainfo, ginfo, FEinfo, idxsamp, Nsample,YList )
%
%     
%     
%      with original *a*
%
%
%
%
%




%% localsolver FE
solloc = cell(mesh_global.nelem,1);
%YList  = cell(mesh_global.nelem,1);


for elemidx = 1:mesh_global.nelem
    PM.vidx = mesh_global.elem(elemidx,:);
    PM.vcoord = mesh_global.coord(PM.vidx,:);
    PM.const = (1/4) * (PM.vcoord(1,2) - PM.vcoord(2,2)) * (PM.vcoord(4,1) - PM.vcoord(1,1));

    solloc{elemidx}  = zeros(mesh_local.ncoord, mesh_local.bd.nbd + 1, Nsample);
    % mesh.bf.nbd + 1 : f =0 + bdprob and f + 0 bdprob

    % define local profile function F no dependence on RF
    F  = assem_F( mesh_local, @STIMA_F, ginfo.int, FEinfo, PM );
    F0 = zeros(mesh_local.ncoord,1); 
    
    %LSd = ainfo.A{elemidx}(:,idxsamp);
    %YList{elemidx} = (1/ainfo.Range) * (lsqlin(ainfo.LSmat{elemidx},LSd-ainfo.Abar{elemidx}))';  % row vector's    

    MAT = assem_Mat( mesh_local, @STIMA_FE, YList{elemidx}, elemidx, ainfo, ginfo.int, FEinfo, PM, idxsamp );
    %MAT = assem_Mat( mesh_local, @STIMA, YList{elemidx}(idxsamp,:), elemidx, ainfo, ginfo.int, FEinfo, PM );

    for idxbdprob = 1:mesh_local.bd.nbd
        idx1 = mesh_local.bd.all(idxbdprob);
        bdv = zeros(mesh_local.ncoord,1);
        bdv(idx1) = 1;
        % F - MAT * bdcond vector
        FF = F0 - MAT * bdv; 
        % Solve non-bd solutions
        solloc{elemidx}(mesh_local.bd.allnot,idxbdprob,idxsamp) = MAT(mesh_local.bd.allnot,mesh_local.bd.allnot)\FF(mesh_local.bd.allnot,1);
        solloc{elemidx}(idx1                ,idxbdprob,idxsamp) = 1;     
    end %idxbdprob
    solloc{elemidx}(mesh_local.bd.allnot,mesh_local.bd.nbd+1,idxsamp) = MAT(mesh_local.bd.allnot,mesh_local.bd.allnot)\F(mesh_local.bd.allnot,1);
    solloc{elemidx}(mesh_local.bd.all   ,mesh_local.bd.nbd+1,idxsamp) = 0;

end % elemidx


%% Interface problem

% length of interface problem - total
ninter = size(mesh_global.coord,1);

% interface problem main matrix
%[MATinter, binter] = assem_Mat_inter( mesh_global, mesh_local, @STIMA_inter, solloc, idxsamp, YList, ainfo, ginfo, FEinfo );
[MATinter, binter] = assem_Mat_inter( mesh_global, mesh_local, @STIMA_inter_FE, solloc, idxsamp, YList, ainfo, ginfo, FEinfo );

% boundary condition of original total domain
bdvinter = zeros(ninter,1);
bdvinter(mesh_global.gbd.idx,1) = FEinfo.g(mesh_global.coord(mesh_global.gbd.idx,:)); 

% Actual solving content vector
B = binter - MATinter * bdvinter;

% solution of interface problem
FES = zeros(ninter,1);
FES(mesh_global.gbd.idxnot,1) = MATinter(mesh_global.gbd.idxnot,mesh_global.gbd.idxnot)\B(mesh_global.gbd.idxnot,1);
FES(mesh_global.gbd.idx   ,1) = bdvinter(mesh_global.gbd.idx,1);

%% build local solution from interface solution
FESall = cell(mesh_global.nelem,1);
for elemidx = 1:mesh_global.nelem
    soltmp = solloc{elemidx}(:,1:mesh_local.bd.nbd,idxsamp) * FES(mesh_global.elemtov(elemidx,:))...
                       + solloc{elemidx}(:,mesh_local.bd.nbd+1,idxsamp); 
    FESall{elemidx} = reshape(soltmp,mesh_local.nor+1,mesh_local.noc+1);
end % elemidx

[ FESall_t ] = switch_size( FESall, mesh_global );







end % function

