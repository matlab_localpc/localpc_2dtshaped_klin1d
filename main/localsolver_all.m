function [ sollocpc ] = localsolver_all( sollocpcsame, mesh, mesh_global, ainfo, ginfo, FEinfo, PCinfo )
%
%   return local solutions of a given RDF variable values.
%
%   rows - mesh location = mesh.ncoord
%   cols - bd problems   = mesh.nbd
%
%
%
%
%

sollocpc = sollocpcsame;

% size size(gridlist,1),size(chart,1);
[ EP_grid ] = Eval_Poly_grid( ginfo.RDF.grid, PCinfo.pol, PCinfo.p, PCinfo.ndim, PCinfo.chart );


MAT = cell(ginfo.RDF.n,1);

PM.vidx = mesh_global.elem(1,:);
PM.vcoord = mesh_global.coord(PM.vidx,:);
PM.const = (1/4) * (PM.vcoord(1,2) - PM.vcoord(2,2)) * (PM.vcoord(4,1) - PM.vcoord(1,1));


for idxRDFgrid = 1:ginfo.RDF.n
    Y   = ginfo.RDF.grid(idxRDFgrid,:);
    MAT{idxRDFgrid} = assem_Mat( mesh, @STIMA, Y, 1, ainfo, ginfo.int, FEinfo, PM );
end % for idxgrid


for elemidx = 1:mesh_global.nelem  
    PM.vidx = mesh_global.elem(elemidx,:);
    PM.vcoord = mesh_global.coord(PM.vidx,:);
    PM.const = (1/4) * (PM.vcoord(1,2) - PM.vcoord(2,2)) * (PM.vcoord(4,1) - PM.vcoord(1,1));

    F  = assem_F( mesh, @STIMA_F, ginfo.int, FEinfo, PM );
    solloc  = zeros(mesh.ncoord, ginfo.RDF.n, 1);
    for idxRDFgrid = 1:ginfo.RDF.n
        solloc(mesh.bd.allnot,idxRDFgrid,1) = MAT{idxRDFgrid}(mesh.bd.allnot,mesh.bd.allnot)\F(mesh.bd.allnot,1);
        solloc(mesh.bd.all,   idxRDFgrid,1) = 0;

    end % for idxgrid
    
    for j = 1:size(PCinfo.chart,1)
        sollocpc{elemidx}(:,j,mesh.bd.nbd+1) = solloc(:,:,1) * ( EP_grid(:,j) .* ginfo.RDF.w );
    end%j
    
    
end % elemidx








end

