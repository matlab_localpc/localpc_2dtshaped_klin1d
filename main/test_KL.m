% Test KL error

NORM_KL    = zeros(7,1);

Gmesh = 4;
Lmesh = 4;
p     = 2;

fname = sprintf('/Users/yichen/Documents/matlabsaves/T_shaped_KL_%d_%d_i_%d.mat',Gmesh,Lmesh,p);


for ny = 1:7
    [NORM_KL(ny)] = KL2DKL( Gmesh,Lmesh,ny,p )
end %ny

 
save(fname);

semilogy((1:7),NORM_KL,'-o');


%% KLE Error
%{
ERR_KL = [4.60e-3, 4.60e-3, 1.36e-4, 1.31e-4, 1.18e-4, 3.51e-7];
semilogy((1:6),ERR_KL,'-o');
title('KLE error, inf norm, 100 samples')
xlabel('number of KLE terms' )
ylabel('ERROR, inf norm(samples) - inf norm(on mesh points)')
%}


%% PCE Error
%{
ERR_PC = [4.09e-7, 1.23e-8, 4.79e-10, 1.88e-11, 4.32e-11, 2.68e-10];
semilogy((1:6),ERR_PC,'-x');
title('PCE error, inf norm, 100 samples')
xlabel('number of PC highest order' )
ylabel('ERROR, inf norm(samples) - inf norm(on mesh points)')
%}

