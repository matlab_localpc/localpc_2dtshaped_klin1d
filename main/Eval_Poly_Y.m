function [ EP ] = Eval_Poly_Y( YList, N, pol, p, ndim, chart )


%% Parameters




%% Update EP
%[ q ] = GET_Multiidx( p,ndim );

EP = zeros(size(chart,1),N);

for i = 1:N
    P = sqrt(1/2) * pol.eval(0:p, YList(i,:));
    for j = 1:size(chart,1)
        EP(j,i) = prod(diag(P((1:ndim),chart(j,:)+1)));
    end %j    
end%i






end
