function [ ainfo ] = ainfo_generator( s, mesh, KLT2D )
%
%   s terms of KL expansion is kept.
%   mesh is mesh_global
%
%


% # of samples
M = 100;

Nq = 20;  % # of quad points on one dim,for KL COV length L = 4e-1

%%{

% Quad rule
pol = OrthPolynomial.Legendre();
qr = pol.quadrule(Nq-2);
points = [-1;qr.x';1];


[U,V] = meshgrid(points, points);
%gridcoord = [U(:) V(:)]'; 
%n = size(x,2);

% load A - random field samples on all grid points
% Rows - points, Col's - samples 

A_1d     = cell(mesh.nelem,1);
Abar_1d  = cell(mesh.nelem,1);
%LSmat = cell(mesh.nelem,1);
ainfo.grid = cell(mesh.nelem,1);
ainfo.U    = cell(mesh.nelem,1);
ainfo.V    = cell(mesh.nelem,1);
ainfo.Vall = cell(mesh.nelem,1);
ainfo.Dall = cell(mesh.nelem,1);
ainfo.n1d  = Nq;

% Horizontal part in T-shaped
for elemidx = 1:mesh.nelem_horizontal    
    vidx = mesh.elem(elemidx,:);
    vcoord = mesh.coord(vidx,:);
    
    ainfo.U{elemidx}   = (vcoord(1,1)+vcoord(3,1))/2 + ((vcoord(3,1)-vcoord(1,1))/2)*U;
    ainfo.V{elemidx}   = (vcoord(1,2)+vcoord(2,2))/2 + ((vcoord(1,2)-vcoord(2,2))/2)*V;
    
    gridcoord = [ainfo.U{elemidx}(:), ainfo.V{elemidx}(:)]; 

    A_1d{elemidx} = zeros(ainfo.n1d,M);
    for sampidx = 1:M
        A_1d{elemidx}(:,sampidx) = interp1(KLT2D.h.x,KLT2D.h.A(:,sampidx),ainfo.U{elemidx}(1,:))';
    end % sampidx 
    
    Abar_1d{elemidx} = (1/M) * sum(A_1d{elemidx},2);
    Atilda_1d = A_1d{elemidx} - repmat(Abar_1d{elemidx},1,M);
    C =  (1/M) * (Atilda_1d * Atilda_1d');
    
    % eigenvectors and eigenvalues
    [ainfo.Vall{elemidx} , ainfo.Dall{elemidx}] = eig(C);
    
    % save info gridcoord
    ainfo.grid{elemidx} = gridcoord;
       
    % same average idea.
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    Abar_1d{elemidx} = KLT2D.h.AVE;
    %%%%%%%%%%%%%%%%%%%%%%%%%%
end % elemidx



% Vertical part in T-shaped
for elemidx = mesh.nelem_horizontal+1:mesh.nelem    
    vidx = mesh.elem(elemidx,:);
    vcoord = mesh.coord(vidx,:);
    
    ainfo.U{elemidx} = (vcoord(1,1)+vcoord(3,1))/2 + ((vcoord(3,1)-vcoord(1,1))/2)*U;
    ainfo.V{elemidx} = (vcoord(1,2)+vcoord(2,2))/2 + ((vcoord(1,2)-vcoord(2,2))/2)*V;
    
    gridcoord = [ainfo.U{elemidx}(:), ainfo.V{elemidx}(:)]; 

    A_1d{elemidx} = zeros(ainfo.n1d,M);
    for sampidx = 1:M
        A_1d{elemidx}(:,sampidx) = interp1(KLT2D.v.y,KLT2D.v.A(:,sampidx),ainfo.V{elemidx}(:,1));
    end % sampidx 
    
    Abar_1d{elemidx} = (1/M) * sum(A_1d{elemidx},2);
    Atilda_1d = A_1d{elemidx} - repmat(Abar_1d{elemidx},1,M);
    C =  (1/M) * (Atilda_1d * Atilda_1d');
    
    % eigenvectors and eigenvalues
    [ainfo.Vall{elemidx} , ainfo.Dall{elemidx}] = eig(C);
    
    % save info gridcoord
    ainfo.grid{elemidx} = gridcoord;
    
    % same average idea.
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    Abar_1d{elemidx} = KLT2D.v.AVE;
    %%%%%%%%%%%%%%%%%%%%%%%%%%
end % elemidx


ainfo.A = A_1d;
ainfo.Abar = Abar_1d;
%ainfo.LSmat = LSmat;

%% Prepare for least square fitting equation LSC*y = LSd
ainfo.Nq = Nq;
ainfo.n = Nq*Nq;

ainfo.LSmat = cell(mesh.nelem,1);

% default, using #1 element
same_elem_idx_h = 1;
same_elem_idx_v = 1;

% pick some number a bit more general, somewhere in the middle
if mesh.noe == 4
    same_elem_idx_h = 22;
    same_elem_idx_v = mesh.nelem_horizontal+12;
elseif mesh.noe == 8
    same_elem_idx_h = 91;
    same_elem_idx_v = mesh.nelem_horizontal+56;
elseif mesh.noe == 16
    same_elem_idx_h = 376;
    same_elem_idx_v = mesh.nelem_horizontal+240;
end

% write LS matrix in to file - ainfo - for later usage.
Veig = ainfo.Vall{same_elem_idx_h}(:,end-s+1:end);
Deig = ainfo.Dall{same_elem_idx_h}(end-s+1:end,end-s+1:end);
for elemidx = 1:mesh.nelem_horizontal   
    ainfo.LSmat{elemidx} = fliplr(Veig) * rot90(sqrt(Deig),2);    
end % elemidx

Veig = ainfo.Vall{same_elem_idx_v}(:,end-s+1:end);
Deig = ainfo.Dall{same_elem_idx_v}(end-s+1:end,end-s+1:end);
for elemidx = mesh.nelem_horizontal+1:mesh.nelem   
    ainfo.LSmat{elemidx} = fliplr(Veig) * rot90(sqrt(Deig),2);    
end % elemidx



% LS solver
%y = lsqlin(LSC,LSd);

% re-construct a's approximation
%Aapprox = LSC * y;

% show the look of ainfo
%{
figure;
for i=1:mesh.nelem
    surf(ainfo.U{i},ainfo.V{i},reshape(ainfo.A{i}(:,1),Nq,Nq));
    hold on;
end
%}    

ainfo.nelem_horizontal = mesh.nelem_horizontal;
    


end

