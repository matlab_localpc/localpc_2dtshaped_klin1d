function [ EP ] = Eval_Poly_grid( gridlist, pol, p, ndim, chart )


%% Parameters




%% Update EP
%[ q ] = GET_Multiidx( p,ndim );

EP = zeros(size(gridlist,1),size(chart,1));

for i = 1:size(gridlist,1)
    P = sqrt(1/2) * pol.eval(0:p, gridlist(i,:));
    for j = 1:size(chart,1)
        EP(i,j) = prod(diag(P((1:ndim),chart(j,:)+1)));
    end %j    
end%i






end
