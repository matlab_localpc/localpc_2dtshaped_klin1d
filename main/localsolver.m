function [ sollocpc ] = localsolver(mesh, ainfo, ginfo, FEinfo, PCinfo )
%
%   return local solutions of a given RDF variable values.
%
%   rows - mesh location = mesh.ncoord
%   cols - bd problems   = mesh.nbd
%
%
%
%
%
solloc  = zeros(mesh.ncoord, ginfo.RDF.n, mesh.bd.nbd + 1);
% mesh.bf.nbd + 1 : f =0 + bdprob and f + 0 bdprob

% size size(gridlist,1),size(chart,1);
[ EP_grid ] = Eval_Poly_grid( ginfo.RDF.grid, PCinfo.pol, PCinfo.p, PCinfo.ndim, PCinfo.chart );


% define local profile function F no dependence on RF
F  = assem_F( mesh, @STIMA_F, ginfo.int, FEinfo );
F0 = zeros(mesh.ncoord,1); 

for idxRDFgrid = 1:ginfo.RDF.n
    Y   = ginfo.RDF.grid(idxRDFgrid,:);
    MAT = assem_Mat( mesh, @STIMA, Y, ainfo, ginfo.int, FEinfo );
    for idxbdprob = 1:mesh.bd.nbd
        % pre-Adjustment
        idx1 = mesh.bd.all(idxbdprob);
        %idx0 = mesh.bd.all;
        %idx0(idxbdprob) = [];
        % bdcond vector
        bdv = zeros(mesh.ncoord,1);
        bdv(idx1) = 1;
        % F - MAT * bdcond vector
        FF = F0 - MAT * bdv; 
        % Solve non-bd solutions
        solloc(mesh.bd.allnot,idxRDFgrid,idxbdprob) = MAT(mesh.bd.allnot,mesh.bd.allnot)\FF(mesh.bd.allnot,1);
        solloc(idx1,          idxRDFgrid,idxbdprob) = 1;
    end % for idxbdprob
    solloc(mesh.bd.allnot,idxRDFgrid,mesh.bd.nbd+1) = MAT(mesh.bd.allnot,mesh.bd.allnot)\F(mesh.bd.allnot,1);
    solloc(mesh.bd.all,   idxRDFgrid,mesh.bd.nbd+1) = 0;
    
end % for idxgrid

sollocpc = zeros( mesh.ncoord, size(PCinfo.chart,1), mesh.bd.nbd+1 );

for idxbdprob = 1:mesh.bd.nbd+1
    for j = 1:size(PCinfo.chart,1)
        sollocpc(:,j,idxbdprob) = solloc(:,:,idxbdprob) * ( EP_grid(:,j) .* ginfo.RDF.w );
    end%j
end % for idxbdprob    




end

