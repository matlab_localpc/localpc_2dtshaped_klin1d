function [ FES ] = FEsolver_total(  mesh_total, mesh_global, mesh_local, ainfo, ginfo, FEinfo, idxsamp )
%
%     
%     
%   with original *a*
%
%
%
%

gnor = mesh_global.noe;
gnoc = mesh_global.noe;

lnor = mesh_local.nor;
lnoc = mesh_local.noc; 

tnor = mesh_total.noe;
tnoc = mesh_total.noe;


%% map idx of sub in mesh_total --> idx of sub in mesh_global
mapsub = zeros(mesh_total.nelem,1);
mapidx = (1:tnor);
idxsubglobal = (1:gnor);
for i = 1:3*gnoc
    for lidxcol = 1:lnoc  
        mapsub(mapidx) = set_Cols(idxsubglobal,lnor);  
        mapidx = mapidx + tnor;
    end %lidxcol
    idxsubglobal = idxsubglobal + gnor;
end % i

mapidx = (mesh_total.nelem_horizontal+1:mesh_total.nelem_horizontal+2*tnor);
idxsubglobal = (mesh_global.nelem_horizontal+1:mesh_global.nelem_horizontal+2*gnor);
for i = 1:gnoc
    for lidxcol = 1:lnoc  
        mapsub(mapidx) = set_Cols(idxsubglobal,lnor);  
        mapidx = mapidx + 2*tnor;
    end %lidxcol
    idxsubglobal = idxsubglobal + 2*gnor;
end % i




%% Get YList from mesh_global info
%{
YList  = cell(mesh_global.nelem,1);
for elemidx = 1:mesh_global.nelem
    LSd = ainfo.A{elemidx}(:,idxsamp);
    YList{elemidx} = (1/ainfo.Range) * (lsqlin(ainfo.LSmat{elemidx},LSd-ainfo.Abar{elemidx}))'; 
end %elemidx
%}


%% Get FE solution of total map
%FES = zeros(mesh_total.ncoord, 1);

% RHS term \int f * v
F = assem_F( mesh_total, @STIMA_F_total, ginfo.int, FEinfo );

% Stiffness matrix  \int a * du * dv
MAT = assem_Mat_total( mesh_total, @STIMA_total, ainfo, ginfo.int, FEinfo, mapsub, idxsamp );

% boundary condition
%{
bdv = zeros(mesh_total.ncoord,1);    
bdv(mesh_total.bd.all,1) = FEinfo.g(mesh_total.coord(mesh_total.bd.all,:)); 

B = F - MAT * bdv;

FES(mesh_total.bd.allnot,1) = MAT(mesh_total.bd.allnot,1)\B(mesh_total.bd.allnot,1);
FES(mesh_total.bd.all,   1) = bdv(mesh_total.bd.all,1);
%}


solbd = zeros(mesh_total.ncoord_global,mesh_total.bd.nbd);
F0 = zeros(mesh_total.ncoord_global,1); 
for idxbdprob = 1:mesh_total.bd.nbd
    idx1 = mesh_total.bd.all(idxbdprob);
    bdv = zeros(mesh_total.ncoord_global,1);
    bdv(idx1) = 1;
    % F - MAT * bdcond vector
    FF = F0 - MAT * bdv; 
    % Solve non-bd solutions
    solbd(mesh_total.bd.allnot,idxbdprob) = MAT(mesh_total.bd.allnot,mesh_total.bd.allnot)\FF(mesh_total.bd.allnot,1);
    solbd(idx1                ,idxbdprob) = 1;     
end %idxbdprob

solf = zeros(mesh_total.ncoord_global,1);
solf(mesh_total.bd.allnot) = MAT(mesh_total.bd.allnot,mesh_total.bd.allnot)\F(mesh_total.bd.allnot,1);
solf(mesh_total.bd.all   ) = 0;

FES = solbd * FEinfo.g(mesh_total.coord(mesh_total.bd.all,:)) + solf;


% To show the look of a FE reference solution
% By bubbles
%{
figure;
scatter3(mesh_total.coord(1:mesh_total.ncoord_global,1),mesh_total.coord(1:mesh_total.ncoord_global,2),FES,'r.');
%scatter3(mesh_total.coord(1:mesh_total.ncoord_horizontal,1),mesh_total.coord(1:mesh_total.ncoord_horizontal,2),FES(1:mesh_total.ncoord_horizontal),'r.');
%}

% By surf plot
%{
xp = linspace(-3,3,tnor*3+1);
yp = linspace(3, 1,tnor+1  );
[U,V] = meshgrid(xp, yp);
Z_h = reshape(FES(1:mesh_total.ncoord_horizontal),tnor+1,tnor*3+1);
figure;
surf(U,V,Z_h);
hold on;

xp = linspace(-1,1,tnor+1);
%yp = linspace(1-2/tnor,-3,tnor*2 );
yp = linspace(1,-3,tnor*2+1 );
[U,V] = meshgrid(xp, yp);
Z_v(2:tnor*2+1,1:tnor+1) = reshape(FES(mesh_total.ncoord_horizontal+1:end),tnor*2,tnor+1);
Z_v(1,:) = Z_h(tnor+1,tnor+1:tnor*2+1);
surf(U,V,Z_v);
%}


end % function


