function [ NORM_PC_all  ] = KL2DPC( Nog,Nol,ny,p  )
%
%
%
%
%
%
%

%%{


Nsample = 100;

Layer.N = 2;

gnor = Nog;
gnoc = Nog;

nskl = Nol;

nor = nskl;
noc = nskl;

%norref = nskl+1;
%nocref = nskl+1;


%% p is the PC order
%p = 2;

%% random field info -- ainfo
% ny = 2;  % # of terms to keep in KLE  
% ny = # of newly invented variable Y [-1,1] uniform each
[ mesh_global ] = mesh_global_T(Nog, nskl );

load('KLT2D_AVE10_L4e-1.mat','KLT2D');

[ ainfo ] = ainfo_generator( ny,mesh_global,KLT2D );
ainfo.Nsample = Nsample;
ainfo.Range = 2.5;

%% grid info for a [-1,1]^2 -- RV domain
%CC_Smolyak_generate(2,7);
[ginfo.int.grid,ginfo.int.w] = ZWsmolyak_load(2, 7);
ginfo.int.N = size(ginfo.int.grid,1);

[ginfo.lint.grid, ginfo.lint.w] = ZWsmolyak_load(1, 5);
ginfo.lint.N = size(ginfo.lint.grid,1);

%% mesh info for local solver
[ mesh_local ] = mesh2d( nor, noc );
%[ meshref ] = mesh2d( norref, nocref );

%% mesh info for FE-total solver
[ mesh_total ] = mesh_global_T( gnor*nor, 1 );




%% finite element derivatives with x - a list of vertices  (n*2)
FEinfo.FEd{1} = @(v) [(-1/4)*(v(:,2)+1), (-1/4)*(v(:,1)-1)];
FEinfo.FEd{2} = @(v) [( 1/4)*(v(:,2)-1), ( 1/4)*(v(:,1)-1)];
FEinfo.FEd{3} = @(v) [( 1/4)*(v(:,2)+1), ( 1/4)*(v(:,1)+1)];
FEinfo.FEd{4} = @(v) [(-1/4)*(v(:,2)-1), (-1/4)*(v(:,1)+1)];

FEinfo.FE{1} = @(v) (-1/4) * ( (v(:,1)-1) .* (v(:,2)+1) );
FEinfo.FE{2} = @(v) ( 1/4) * ( (v(:,1)-1) .* (v(:,2)-1) );
FEinfo.FE{3} = @(v) ( 1/4) * ( (v(:,1)+1) .* (v(:,2)+1) );
FEinfo.FE{4} = @(v) (-1/4) * ( (v(:,1)+1) .* (v(:,2)-1) );

FEinfo.f = @(v) ones(size(v,1),1);
%FEinfo.f = @(v) 2 + ( v(:,1) .* v(:,2) );
%FEinfo.f = @(v) -16 * 4 * ones(size(v,1),1);
%FEinfo.f = @(v) -16 * 4 * ones(size(v,1),1);

FEinfo.g = @(v) zeros(size(v,1),1);
%FEinfo.g = @(v) 2 +  (v(:,1).^3) .* (v(:,2));
%FEinfo.g = @(v) 3* (v(:,1).^2) + 5*(v(:,2).^2);
%FEinfo.g = @(v) 3* (v(:,1).^3) + 5*(v(:,2).^2);


%% Random field Variables
%CC_Smolyak_generate(ny,6);
[ginfo.RDF.grid,ginfo.RDF.w] = ZWsmolyak_load(ny, 7);
ginfo.RDF.n = size(ginfo.RDF.grid,1);

ndim = ny;
[PCinfo.chart ] = GET_Multiidx( p,ndim );

%% PC preparation
pol = OrthPolynomial.Legendre();
PCinfo.pol = pol;
PCinfo.p = p;
PCinfo.ndim = ndim;


%% Local solver
% local solution is #of pts on FE mesh, # of boundary problems, # of RDF sparse grids 
% size : mesh.ncoord * size(ginfo.chart,1) * mesh.nbd + 1
sollocpcsamelayer = cell(2,1); % 2 layers, horizontal:1 or vertical:2
sollocpcsame = cell(mesh_global.nelem,1);
%sollocpc = cell(mesh_global.nelem,1);
solPCbd  = cell(mesh_global.nelem,1);
YList    = cell(mesh_global.nelem,1);


%%{
[ sollocpcsamelayer{1} ] = localsolver_same(1,                              mesh_local, mesh_global, ainfo, ginfo, FEinfo, PCinfo);
[ sollocpcsamelayer{2} ] = localsolver_same(mesh_global.nelem_horizontal+1, mesh_local, mesh_global, ainfo, ginfo, FEinfo, PCinfo);


for elemidx = 1:mesh_global.nelem_horizontal
    layeridx = 1;
    sollocpcsame{elemidx} = sollocpcsamelayer{layeridx};
end % elemidx

for elemidx = mesh_global.nelem_horizontal+1:mesh_global.nelem
    layeridx = 2;
    sollocpcsame{elemidx} = sollocpcsamelayer{layeridx};
end % elemidx


[ sollocpc ] = localsolver_all( sollocpcsame, mesh_local, mesh_global, ainfo, ginfo, FEinfo, PCinfo );
%}

for elemidx = 1:mesh_global.nelem
    %[ sollocpc{elemidx} ] = localsolver_id(elemidx, mesh_local, mesh_global, ainfo, ginfo, FEinfo, PCinfo);    
    % Re-consruction the solution for each sample
    solPCbd{elemidx} = zeros(mesh_local.ncoord, mesh_local.bd.nbd+1, Nsample);

    for idxsamp = 1:Nsample
        % PC solutions
        LSd = ainfo.A{elemidx}(:,idxsamp);
        % Get Y
        YList{elemidx}(idxsamp,:) = (1/ainfo.Range) * (lsqlin(ainfo.LSmat{elemidx},LSd-ainfo.Abar{elemidx}))';  % row vector's    
        
        %%{
        EP_Y  = Eval_Poly_Y( YList{elemidx}(idxsamp,:), size(YList{elemidx}(idxsamp,:),1), pol, p, ndim, PCinfo.chart );
        for idxbdprob = 1:mesh_local.bd.nbd+1
            solPCbd{elemidx}(:,idxbdprob,idxsamp) = sollocpc{elemidx}(:,:,idxbdprob) * EP_Y;
        end % for idxbdprob    
        %}
        
        % FE reference solutions
    end % for idxsamp

end % elemidx



%% interface problem

PCS_re_t     = cell(Nsample,1);
FES_t_origa  = cell(Nsample,1);
FES_lg_appra = cell(Nsample,1);
FES_lg_origa = cell(Nsample,1);

ERR_TOTAL = cell(Nsample,1);
ERR_KL    = cell(Nsample,1);
ERR_PC    = cell(Nsample,1);
ERR_KL_std = cell(Nsample,1);

NORM_TOTAL = zeros(Nsample,1); 
NORM_KL    = zeros(Nsample,1);
NORM_PC    = zeros(Nsample,1);
NORM_KL_std = zeros(Nsample,1);

for idxsamp = 1:Nsample
    % length of interface problem - total
    ninter = size(mesh_global.coord,1);

    %%{
    % interface problem main matrix
    [MATinter, binter] = assem_Mat_inter( mesh_global, mesh_local, @STIMA_inter, solPCbd, idxsamp, YList, ainfo, ginfo, FEinfo );
    
    % boundary condition of original total domain
    bdvinter = zeros(ninter,1);
    bdvinter(mesh_global.gbd.idx,1) = FEinfo.g(mesh_global.coord(mesh_global.gbd.idx,:)); 
    
    % Actual solving content vector
    B = binter - MATinter * bdvinter;
     
    % solution of interface problem
    solinter = zeros(ninter,1);
    solinter(mesh_global.gbd.idxnot,1) = MATinter(mesh_global.gbd.idxnot,mesh_global.gbd.idxnot)\B(mesh_global.gbd.idxnot,1);
    solinter(mesh_global.gbd.idx   ,1) = bdvinter(mesh_global.gbd.idx,1);

    % show global solution
    %scatter3(mesh_global.coord(:,1),mesh_global.coord(:,2),solinter)
    
    % #1 solution
    % reconstruct PC solution to total mesh
    [ PCS_re_t{idxsamp} ] = FEreconstruct( solinter, mesh_global, mesh_local, YList, ainfo, ginfo, FEinfo, idxsamp );
    %}
    
    % #2 solution
    % FE solution with original a and total mesh
    %[ FES_t_origa{idxsamp} ] = FEsolver_total(  mesh_total, mesh_global, mesh_local, ainfo, ginfo, FEinfo, idxsamp );

    % #3 solution
    % FE solution with original a on global mesh and interface problem
    % than, switch to mesh_total
    %[ ~, FES_lg_origa{idxsamp} ] = FEsolver_a( mesh_global, mesh_local, ainfo, ginfo, FEinfo, idxsamp, Nsample,YList );
 
    % #4 solution
    % FE solution with approximated a on global mesh and interface problem  
    % than, switch to mesh_total
    [ ~, FES_lg_appra{idxsamp} ] = FEsolver( mesh_global, mesh_local, ainfo, ginfo, FEinfo, idxsamp, Nsample, YList);
    
    
    %ERR_TOTAL{idxsamp}  = PCS_re_t{idxsamp}(:) - FES_t_origa{idxsamp};    
    %ERR_KL{idxsamp}     = FES_t_origa{idxsamp} - FES_lg_appra{idxsamp}(:);    
    %ERR_KL_std{idxsamp} = FES_t_origa{idxsamp} - FES_lg_origa{idxsamp}(:);
    ERR_PC{idxsamp}     = PCS_re_t{idxsamp}(:) - FES_lg_appra{idxsamp}(:);
    
    
    %NORM_TOTAL(idxsamp,1)  = norm(ERR_TOTAL{idxsamp},inf);
    %NORM_KL(idxsamp,1)     = norm(ERR_KL{idxsamp},inf);
    %NORM_KL_std(idxsamp,1) = norm(ERR_KL_std{idxsamp},inf);
    NORM_PC(idxsamp,1)     = norm(ERR_PC{idxsamp},inf);    
    
    
end % for idxsamp


%NORM_TOTAL_all = norm(NORM_TOTAL,inf);
%NORM_KL_all     = norm(NORM_KL,inf);
%NORM_KL_std_all = norm(NORM_KL_std,inf);
NORM_PC_all    = norm(NORM_PC,inf);


%savename = sprintf('/Users/yichen/Documents/matlabsaves/2D_layer_L2e-1_G%d_L%d_ny%d_p%d.mat',Nog,Nol,ny,p);
%save(savename);


end% function
