function [ sollocpc ] = localsolver_id(elemidx, sollocpcsame, mesh, mesh_global, ainfo, ginfo, FEinfo, PCinfo )
%
%   return local solutions of a given RDF variable values.
%
%   rows - mesh location = mesh.ncoord
%   cols - bd problems   = mesh.nbd
%
%
%
%
%

PM.vidx = mesh_global.elem(elemidx,:);
PM.vcoord = mesh_global.coord(PM.vidx,:);
PM.const = (1/4) * (PM.vcoord(1,2) - PM.vcoord(2,2)) * (PM.vcoord(3,1) - PM.vcoord(1,1));

solloc  = zeros(mesh.ncoord, ginfo.RDF.n, 1);
% mesh.bf.nbd + 1 : f =0 + bdprob and f + 0 bdprob

% size size(gridlist,1),size(chart,1);
[ EP_grid ] = Eval_Poly_grid( ginfo.RDF.grid, PCinfo.pol, PCinfo.p, PCinfo.ndim, PCinfo.chart );


% define local profile function F no dependence on RF
F  = assem_F( mesh, @STIMA_F, ginfo.int, FEinfo, PM );
%F0 = zeros(mesh.ncoord,1); 

for idxRDFgrid = 1:ginfo.RDF.n
    Y   = ginfo.RDF.grid(idxRDFgrid,:);
    MAT = assem_Mat( mesh, @STIMA, Y, elemidx, ainfo, ginfo.int, FEinfo, PM );

    solloc(mesh.bd.allnot,idxRDFgrid,1) = MAT(mesh.bd.allnot,mesh.bd.allnot)\F(mesh.bd.allnot,1);
    solloc(mesh.bd.all,   idxRDFgrid,1) = 0;
    
end % for idxgrid

%sollocpc = zeros( mesh.ncoord, size(PCinfo.chart,1), mesh.bd.nbd+1 );
sollocpc = sollocpcsame;


for idxbdprob = mesh.bd.nbd+1:mesh.bd.nbd+1
    for j = 1:size(PCinfo.chart,1)
        sollocpc(:,j,idxbdprob) = solloc(:,:,1) * ( EP_grid(:,j) .* ginfo.RDF.w );
    end%j
end % for idxbdprob    




end

