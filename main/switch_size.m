function [ FESall_list ] = switch_size( FESall, mesh_global )
%
%
%    switch solution of local mesh to global_total mesh
%
%
%
%
%
%

% get info
nelem = mesh_global.nelem;
nelem_h = mesh_global.nelem_horizontal;
nelem_v = mesh_global.nelem_vertical;

%nor = mesh_global.noe;
%noc = mesh_global.noe;
noe = mesh_global.noe;



len = size(FESall{1},1);
n = len - 1;


% build map
mapelem = zeros(nelem,2);
mapelem(1:nelem_h,1) = set_Rows((1:noe),noe*3);
mapelem(1:nelem_h,2) = set_Cols((1:noe*3),noe);

mapelem(nelem_h+1:nelem,1) = set_Rows((noe+1:3*noe),noe);
mapelem(nelem_h+1:nelem,2) = set_Cols((noe+1:2*noe),2*noe);


mapelem = mapelem-1;  %%% important


FESall_t = zeros(n*noe*3+1,n*noe*3+1);

for elemidx = 1:nelem
    ridx = mapelem(elemidx,1);
    cidx = mapelem(elemidx,2); 
    FESall_t(ridx*n+1:ridx*n+n+1,cidx*n+1:cidx*n+n+1) = FESall{elemidx};    
end %elemidx


noh = (n*noe+1)*(n*noe*3+1);
nov = (n*noe*2)*(n*noe+1);
FESall_list = zeros(noh+nov,1);
tmp = FESall_t(1:(n*noe+1),1:(n*noe*3+1));
FESall_list(1:noh,1) = tmp(:);
tmp = FESall_t((n*noe+1)+1:(n*noe*3+1),(n*noe+1):(n*noe*2+1));
FESall_list(noh+1:noh+nov,1) = tmp(:);




end

