function set = ABSConstruct(eta,tol)
%ABSCONSTRUCT construct index set by ABS method
%   eta is ivec of norms of a_m
%   tol is tolerance
  
% C.J. Gittelson / SAM ETHZ / 2011

  % recursively construct index set as a dynamic list
  list = construct(eta,0,tol);
  listc = list.read();
  
  % transform to an FNSetS data structure
  set = FNSetS(listc{:});

end



% inner function for recursive construction
function [list,etanu] = construct(eta,m0,tol)

  % initialize
  list = ilist();
  etanu = ilist();

  % handle exceptional cases
  eta1 = eta.get(m0+1);
  if(eta1<tol)
    if(tol>1)
      return
    else
      list.add(zeros(2,0));
      etanu.add(1);
      return
    end
  end
  
  % determine highest power of eta1
  N = floor(log(tol)/log(eta1));
  
  % recursively construct tails of indices
  for n=0:N
    eta1n = eta1^(-n);
    [taillist,tailetanu] = construct(eta,m0+1,tol*eta1n);
    % add to list
    taillistelems = taillist.read();
    tailetanuvals = tailetanu.read();
    newlist = cellfun(@(mu) [[m0+1;n],mu],taillistelems,'UniformOutput',false);
    newetanu = cellfun(@(x) eta1n*x,tailetanuvals);
    cellfun(@list.add,newlist);
    arrayfun(@etanu.add,newetanu);
  end

end