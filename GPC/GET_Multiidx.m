function [ q ] = GET_Multiidx( p,ndim )

% Input:  ndim - dimensionality
%         p    - highest order
% Output: q    - table of multiindices  

%{ 
for example:
[ q ] = GET_Multiidx( 2,3 )

q =

     0     0     0
     1     0     0
     1     1     0
     1     0     1
     2     0     0
     0     1     0
     0     1     1
     0     2     0
     0     0     1
     0     0     2
%}


set = TDConstruct(p,ndim);

q = zeros(set.N,ndim);

for i = 1:set.N
    tmp = set.multi(i);
    q(i,tmp(1,:)) = tmp(2,:);
end




end % function

