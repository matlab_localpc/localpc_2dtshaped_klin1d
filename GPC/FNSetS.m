classdef FNSetS
%FNSETS index set
%   Detailed explanation goes here
  
% C.J. Gittelson / SAM ETHZ / 2010
  
  properties
    N=0;  % number of elements in index set
  end
  
  properties (GetAccess=protected, SetAccess=protected)
    list % map index pointer to multiindex
    map % map multiindex to index pointer
  end
  
  
  
  methods
    
    function set = FNSetS(varargin)
    % constructor, argument is list of multiindices
    % multiindices are 2xm arrays, dimensions in first row, values in second.
    
      % clean up input and construct character array keys
      mus = cellfun(@FNSetS.cleanup,varargin,'UniformOutput',false);
      keys0 = cellfun(@FNSetS.getKey,mus,'UniformOutput',false);
      [keys,i] = unique(keys0);
      
      % define values
      set.N = length(keys);
      vals = num2cell(1:set.N);
      
      % construct list
      set.list = mus(i);%{mus{i}};
      
      % construct map
      if(isempty(keys))
        set.map = containers.Map();
      else
        set.map = containers.Map(keys,vals);
      end
    
    end
    
    
    function i = intp(set,mu)
    % get integer pointer to multiindex mu
    % returns 0 if not in index set
      key = FNSetS.getKey(FNSetS.cleanup(mu));
      if(set.map.isKey(key))
        i = set.map(key);
      else
        i = 0;
      end
    end
    
    function mu = multi(set,i,envelope)
    % get multiindex with given index pointer
      if(nargin<3 || isempty(envelope))
        envelope = false;
      end
      if(isempty(i))
        mu = {};
      elseif(isscalar(i) && ~envelope)
        mu = set.list{i};
      else
        mu = set.list(i); % {set.list{i}};
      end
    end
    
    function neighbors = neighborGraph(set,varargin)
    % construct graph of neighbors: sparse matrix such that
    %   neighbors(i,j) = m  if  j = i + e_m, 0 otherwise
    % varargin can be maximal dimension M or empty
    
      % initialize
      Ilist = cell(1,set.N);
      Jlist = cell(1,set.N);
      Mlist = cell(1,set.N);
      
      % loop over indices
      for j=1:set.N
        % construct smaller neighbors
        [mus,m] = set.constructNeighborsM(set.multi(j),varargin{:});
        keys = cellfun(@FNSetS.getKey,mus,'UniformOutput',false);
        if(~iscell(keys))
          keys = {keys};
        end
        in = set.map.isKey(keys);
        i = cell2mat(set.map.values(keys(in)));%{keys{in}} %cellfun(@set.intp,{keys{in}});
        Ilist{j} = i;
        Jlist{j} = j(ones(size(i)));
        Mlist{j} = m(in);
      end
      
      % construct graph
      I = [Ilist{:}];
      J = [Jlist{:}];
      M = [Mlist{:}];
      neighbors = sparse(I,J,M,set.N,set.N);
    
    end
    
    function [i,mun] = slice(set,mu,dims)
    % get all elements of set that differ from mu only in dimensions dims
      Xdims = cellfun(@(nu) set.dimsdiffer(mu,nu),set.list,'UniformOutput',false);
      xdims = cellfun(@(ms) setdiff(ms,dims),Xdims,'UniformOutput',false);
      inslice = cellfun(@isempty,xdims);
      i = find(inslice);
      if(nargout>=2)
        mus = set.multi(i,true);
        mun = nan(numel(i),numel(dims));
        for j=1:numel(dims)
          mun(:,j) = cellfun(@(mu) double(FNSetS.read(mu,dims(j))),mus);
        end
      end
    end
    
  end
  
  
  
  methods (Static=true)
    
    function neighbors = constructNeighborsP(mu,M)
    % construct list of all indices one larger than mu in a dim 1:M
      neighbors = arrayfun(@(m) FNSetS.augmentMu(mu,m),1:M,'UniformOutput',false);
    end
    
    function [neighbors,m] = constructNeighborsM(mu,M)
    % construct list of all indices one smaller than mu in a dim 1:M
      mulength = size(mu,2);
      if(nargin<2 || isempty(M))
        ndims = mulength;
      else
        ndims = nnz(mu(1,:)<=M);
      end
      [neighbors{1:ndims}] = deal(mu);
      for i=1:ndims
        if(mu(2,i)>1)
          neighbors{i}(2,i) = mu(2,i)-1;
        else
          neighbors{i} = mu(:,[1:i-1,i+1:mulength]);
        end
      end
      m = double(mu(1,1:ndims));
    end
    
    function mu0 = zero()
    % zero multiindex
      mu0 = uint16(zeros(2,0));
    end
    
    function mum = read(mu,m)
    % get value of multiindex mu in dimension m
      i = find(mu(1,:)==m);
      if(i)
        mum = mu(2,i);
      else
        mum = 0;
      end
    end
    
    function set = union(varargin)
    % construct set as union of sets
      Mus = cellfun(@(set) set.multi(1:set.N,true),varargin,'UniformOutput',false);
      mus = horzcat(Mus{:});
      set = FNSetS(mus{:});
    end
    
  end
  
  
  methods (Access=protected, Static=true)
    
    function mu = cleanup(Mu)
    % convert multiindex to uint16 and remove zeros
      mu = uint16(Mu(:,Mu(2,:)>0));
    end
    
    function key = getKey(mu)
    % find key of multiindex
      key = char(mu(:)');
    end
    
    function mu = readKey(key)
    % find multiindex from key
      mu = uint16(reshape(key,2,[]));
    end
    
    function mu = augmentMu(mu,m)
    % add one to mu in dimension m
      i = find(mu(1,:)==m);
      if(isempty(i))
        mu1 = [mu,[uint16(m);uint16(1)]];
        [~,j] = sort(mu1(1,:));
        mu = mu1(:,j);
      else
        mu(2,i) = mu(2,i) + uint16(1);
      end
    end
    
    function m = dimsdiffer(mu,nu)
    % get dimensions in which mu and nu differ
      xsupp = setxor(mu(1,:),nu(1,:));
      [isupp,imu,inu] = intersect(mu(1,:),nu(1,:));
      inteq = (mu(2,imu)==nu(2,inu));
      m = sort([xsupp,isupp(~inteq)]);
    end
    
  end
    
end

