function set = TDConstruct(p,ndim,weight)
%TDCONSTRUCT Construct set of total degree multiindices
%   the argument weight is optional

% C.J. Gittelson / 2012

  % determine default input
  if(nargin<3 || isempty(weight))
    weight = ones(1,ndim);
  end

  % recursively construct index set as a dynamic list
  list = construct(p,ndim,weight);
  listc = list.read();
  
  % transform to an FNSetS data structure
  set = FNSetS(listc{:});

end

% inner function to construct a slice with degree exactly p
function list = construct(p,ndim,weight)

  % initialize
  list = ilist();
  
  % termination criterion
  if(ndim==0)
    list.add(zeros(2,0));
    return
  end

  % determine highest polynomial degree in first variable
  N = floor(p/weight(1));
  
  % recursively construct tails of indices
  for n=0:N
    taillist = construct(p-weight(1)*n,ndim-1,weight(2:end));
    % add to list
    taillistelems = taillist.read();
    newlist = cellfun(@(mu) [[1;n],[mu(1,:)+1;mu(2,:)]],taillistelems,'UniformOutput',false);
    cellfun(@list.add,newlist);
  end

end