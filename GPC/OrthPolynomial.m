classdef OrthPolynomial
  %ORTHPOLYNOMIAL family of orthonormal polynomials
  %   Detailed explanation goes here
  
% C.J. Gittelson / SAM ETHZ / 2010
  
  properties
    beta % coefficient in recursion
    asym = false; % asymmetric polynomial: need alpha
    alpha = []; % coefficient in recursion for asymmetric densities
    weight = []; % weight (density) of the measure
    sup = []; % maximal value of polynomial (for degrees at least 1)
  end
  
  methods
    
    function pol = OrthPolynomial(bhandle,ahandle,whandle,mhandle,varargin)
    % constructor, arguments: function handle for generating beta and alpha
    
      pol.beta = ivec(bhandle);
      if(nargin>=2 && ~isempty(ahandle))
        pol.asym = true;
        pol.alpha = ivec(ahandle);
      end
      if(nargin>=3 && ~isempty(whandle)) % density function
        pol.weight = whandle;
      end
      if(nargin>=4 && ~isempty(mhandle)) % maximum
        pol.sup = ivec(mhandle);
      end
    
    end
    
    
    function z = eval(pol,n,x)
    % evaluate polynomials by 3-term recursion
    
      % initialize
      nmax = max(n(:));
      nx = numel(x);
      Z = nan(nx,nmax+1);
      Z(:,1) = 1; % constants
      
      % compute values by recursion
      if(nmax>=1)
        if(pol.asym) % asymmetric
          Z(:,2) = (1/pol.beta.get(1))*(x(:)-pol.alpha.get(1)); % linears
          for k=2:nmax % note: index of alpha shifted by one compared to theory
            Z(:,k+1) = (1/pol.beta.get(k))*((x(:)-pol.alpha.get(k)).*Z(:,k) - pol.beta.get(k-1)*Z(:,k-1));
          end
        else % symmetric, ie. alpha=0
          Z(:,2) = (1/pol.beta.get(1))*x(:); % linears
          for k=2:nmax
            Z(:,k+1) = (1/pol.beta.get(k))*(x(:).*Z(:,k) - pol.beta.get(k-1)*Z(:,k-1));
          end
        end 
      end
      
      % extract desired values
      z = Z(:,n+1);
      z = squeeze(reshape(z,[size(x),size(n)]));
      if(numel(x)==1)
        z = shiftdim(z,-1);
      end
    
    end
    
    
    function qr = quadrule(pol,n)
    % construct Gaussian quadrature rule
    
      % assemble matrix of recursion coefficients
      b = arrayfun(@(k) pol.beta.get(k),1:n-1);
      if(pol.asym)
        a = arrayfun(@(k) pol.alpha.get(k),1:n);
      else
        a = zeros(1,n);
      end
      bab = horzcat(vertcat(b',0),a',vertcat(0,b'));
      J = full(spdiags(bab,[-1,0,1],n,n));
      
      % compute eigenvalues and eigenvectors
      [V,D] = eig(J);
      
      % extract quadrature nodes
      qr.x = diag(D)';
      
      % extract quadrature weights
      P = pol.eval(0:n-1,qr.x);
      s = diag(P*V)';
      qr.w = abs(V(1,:)./s);
    
    end
    
  end
  
  
  methods (Static=true)
    
    function pol = Hermite()
    % construct Hermite polynomials 
      whandle = @(x) (1/sqrt(2*pi))*exp(-x.^2/2);
      bhandle = @sqrt;
      pol = OrthPolynomial(bhandle,[],whandle);  
    end
    
    
    function pol = Legendre()
    % construct Legendre polynomials 
      w = 0.5;
      pol = OrthPolynomial( @(n) 1/sqrt(4-double(n)^(-2)),[],@(x) w(size(x)), @(n) sqrt(2*n+1) );
    end
    
    
    function pol = Chebyshev(type)
    % construct Chebyshev polynomials
      switch type
        case 1
          bhandle = @(n) OrthPolynomial.n1Exception(n, 1/sqrt(2), @(m) 0.5);
          whandle = @(x) 1./(pi*sqrt(1-x.^2));
          mhandle = @(n) sqrt(2);
          pol = OrthPolynomial(bhandle,[],whandle,mhandle);
        case 2
          bhandle = @(n) 0.5;
          whandle = @(x) (2/pi)*sqrt(1-x.^2);
          pol = OrthPolynomial(bhandle,[],whandle);
        case 3
          bhandle = @(n) 0.5;
          ahandle = @(n) OrthPolynomial.n1Exception(n, 0.5, @(m) 0);
          whandle = @(x) (1/pi)*sqrt((1+x)./(1-x));
          pol = OrthPolynomial(bhandle,ahandle,whandle);
        case 4
          bhandle = @(n) 0.5;
          ahandle = @(n) OrthPolynomial.n1Exception(n, -0.5, @(m) 0);
          whandle = @(x) (1/pi)*sqrt((1-x)./(1+x));
          pol = OrthPolynomial(bhandle,ahandle,whandle);
      end
    end
    
    function pol = Gegenbauer(lambda)
    % construct Gegenbauer polynomials
      bone = 1/sqrt(2+2*lambda);
      bother = @(n) 0.5*sqrt((n*(n+2*lambda-1))/((n+lambda)*(n+lambda-1)));
      bhandle = @(n) OrthPolynomial.n1Exception(n,bone,bother);
      cw = gamma(lambda+1)/(sqrt(pi)*gamma(lambda+0.5));
      whandle = @(x) cw*(1-x.^2).^(lambda-0.5);
      pol = OrthPolynomial(bhandle,[],whandle);
    end
    
    function pol = Jacobi(a,b)
    % construct Jacobi polynomials
      % weight
      cw = 2^(-a-b-1)*gamma(a+b+1)/(gamma(a+1)*gamma(b+1));
      whandle = @(x) cw*((1-x).^a).*((1+x).^b);
      % coefficient alpha
      if(a==b)
        ahandle = [];
      else
        aone = (b-a)/(a+b+2);
        aother = @(n) (b^2-a^2)/((2*n+1+b)*(2*n+a+b+2));
        ahandle = @(n) OrthPolynomial.n1Exception(n,aone,aother);
      end
      % coefficient beta
      bone = 2*sqrt(((a+1)*(b+1))/((a+b+2)^2*(a+b+3)));
      bother = @(n) 2*sqrt((n*(n+a)*(n+b)*(n+a+b))/((2*n+a+b)^2*(2*n+a+b+1)*(2*n+a+b-1)));
      bhandle = @(n) OrthPolynomial.n1Exception(n,bone,bother);
      % construct polynomial structure
      pol = OrthPolynomial(bhandle,ahandle,whandle);
    end
    
    function pol = Laguerre()
    % construct Laguerre polynomials
      bhandle = @(n) -n;
      ahandle = @(n) 2*n-1;
      whandle = @(x) exp(-x).*(x>=0);
      pol = OrthPolynomial(bhandle,ahandle,whandle);
    end
    
  end
  
 
  methods (Access=protected, Static=true)
    
    function z = n1Exception(n,one,other)
    % piecewise defined function: different definition for n=1
      if(n==1)
        z = one;
      else
        z = other(n);
      end
    end
    
  end
  
end

