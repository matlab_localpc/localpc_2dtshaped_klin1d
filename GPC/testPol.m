% test script for orthonormal polynomials
  
% C.J. Gittelson / SAM ETHZ / 2010

%% Generate and plot Hermite polynomials

pol = OrthPolynomial.Hermite();
x = linspace(-3,3)';
n = 0:5;
P = pol.eval(n,x);

figure;
plot(x,P);
grid on;
title('Hermite Polynomials');


%% Generate and plot Legendre polynomials

pol = OrthPolynomial.Legendre();
x = linspace(-1,1)';
n = 0:5;
P = pol.eval(n,x);

figure;
plot(x,P);
grid on;
title('Legendre Polynomials');


%% Generate and plot Chebyshev polynomials

type = 1;
pol = OrthPolynomial.Chebyshev(type);
x = linspace(-1,1)';
n = 0:5;
P = pol.eval(n,x);

figure;
plot(x,P);
grid on;
title(sprintf('Chebyshev Polynomials, type %d',type));


%% Generate and plot Gegenbauer polynomials

lambda = exp(1);
pol = OrthPolynomial.Gegenbauer(lambda);
x = linspace(-1,1)';
n = 0:5;
P = pol.eval(n,x);

figure;
plot(x,P);
grid on;
title(sprintf('Gegenbauer Polynomials, lambda = %g',lambda));


%% Generate and plot Jacobi polynomials

a = 1;
b = -0.5;
pol = OrthPolynomial.Jacobi(a,b);
x = linspace(-1,1)';
n = 0:3;
P = pol.eval(n,x);

figure;
plot(x,P);
grid on;
title(sprintf('Jacobi Polynomials, a = %g, b= %g',a,b));


%% Generate and plot Laguerre polynomials

pol = OrthPolynomial.Laguerre();
x = linspace(0,10);
n = 0:5;
P = pol.eval(n,x);

figure;
plot(x,P);
grid on;
title('Laguerre Polynomials');
set(gca,'XLim',[0 10],'YLim',[-10 20]);

